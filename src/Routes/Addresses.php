<?php

namespace App\Routes;

use App\Models\User;
use App\Models\UserAddresses;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Zend\Diactoros\Response\JsonResponse;

class Addresses
{
    private EntityManager $connection;
    private Client $client;
    private Environment $twig;
    private Session $session;

    public function __construct(EntityManager $entityManager, Client $client, Environment $twig, Session $session)
    {
        $this->connection = $entityManager;
        $this->client = $client;
        $this->twig = $twig;
        $this->session = $session;
    }

    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $response = new \Zend\Diactoros\Response;

        $userAddressRepository = $this->connection->getRepository(UserAddresses::class);
        $addresses = $userAddressRepository->findBy([
            'user' => $this->session->get('user')
        ]);

        $res = $this->twig->render('pages/addresses.twig', [
            'addresses' => $addresses
        ]);
        $response->getBody()->write($res);

        return $response;
    }

    public function add(ServerRequestInterface $request): ResponseInterface
    {
        $response = new \Zend\Diactoros\Response;
        $postedAddress = $request->getParsedBody()['address'];
        $user = $this->connection->getRepository(User::class)->findOneBy(['id' => $this->session->get('user')->getId()]);

        $address = new UserAddresses();
        $address->setAddress($postedAddress);
        $address->setUser($user);

        $this->connection->persist($address);
        $this->connection->flush($address);

        return $this->index($request);
    }

}