<?php

use App\Eth\EthClient;
use App\Eth\Pools\PancakePool;
use App\Eth\Pools\PancakeRouter;
use App\Routes\Front;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Twig\Environment;
use Zend\Diactoros\Uri;

require_once __DIR__ . '/vendor/autoload.php';
ini_set("memory_limit", "-1");
set_time_limit(-1);
$client = new \GuzzleHttp\Client();

$container = new Container();

// fallback reflection container
$container->delegate(new ReflectionContainer());



$conn = array(
    'dbname' => 'pryzm',
    'user' => 'mysqlprizmd',
    'password' => '',
    'host' => '',
    'driver' => 'pdo_mysql',
);




$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;


$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);
$loader = new \Twig\Loader\FilesystemLoader(['templates']);
$twig = new Environment($loader);
$container->add(Environment::class, $twig);


$strategy = (new League\Route\Strategy\ApplicationStrategy)->setContainer($container);

/** @var \League\Route\Router $router */
$router = (new League\Route\Router)->setStrategy($strategy);

$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/Models",), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

$config->setNamingStrategy(new \Doctrine\ORM\Mapping\UnderscoreNamingStrategy(CASE_LOWER, true));

$entityManager = EntityManager::create($conn, $config);
$connection = $entityManager->getConnection();
$connection->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');


$container->add(Connection::class, $connection);
$container->add(EntityManager::class, $entityManager);

$router->map('POST', '/front/register', 'App\Routes\Front::register');
$router->map('GET', '/front/create', 'App\Routes\Front::create');
$router->map('POST', '/front/login', 'App\Routes\Front::login');

$router->map('GET', '/user/addresses', 'App\Routes\Addresses::index');
$router->map('POST', '/user/addresses', 'App\Routes\Addresses::add');
$router->map('GET', '/user/portfolio', 'App\Routes\Portfolio::index');

$router->map('POST', '/user/xhrproxy', 'App\Routes\XhrProxy::index');

$router->map('GET', '/lpcharts', 'App\Routes\Front::lpCharts');
$router->map('GET', '/lpcharts/{network}', 'App\Routes\Front::lpCharts');
$router->map('GET', '/getprices/{amt}', 'App\Routes\Front::getprices');
$router->map('GET', '/getpricesnetwork/{network}/{amt}', 'App\Routes\Front::getpricesnetwork');
$router->map('GET', '/dopools', 'App\Routes\Front::dopools');
$router->map('GET', '/doprices', 'App\Routes\Front::doprices');
$router->map('GET', '/updatefarms', 'App\Routes\Front::updatefarms');
$router->map('GET', '/updatestats', 'App\Routes\Front::updateStats');
$router->map('GET', '/lp/detail/{id}', 'App\Routes\Front::getDetail');
$router->map('GET', '/dashboard', 'App\Routes\Front::getDashboard');
$router->map('GET', '/lp/getfavorites/{address}', 'App\Routes\Front::getFavorites');
$router->map('GET', '/admn/mngPools', 'App\Routes\Admin::index');
$router->map('POST', '/admn/mngPools/p', 'App\Routes\Admin::indexPost');
$router->map('GET', '/admn/mngFarms', 'App\Routes\Admin::indexFarms');
$router->map('POST', '/admn/mngFarms/p', 'App\Routes\Admin::indexFarmsPost');
$router->map('GET', '/sitemap', 'App\Routes\Admin::sitemap');
$router->map('GET', '/lp/table', 'App\Routes\Table::index');
$router->map('GET', '/douni', 'App\Routes\Front::douni');
$router->map('GET', '/dokoge', 'App\Routes\Front::dokoge');
$router->map('GET', '/wallet', 'App\Routes\Wallet::showWallet');
$router->map('GET', '/wallet/{address}', 'App\Routes\Wallet::showWallet');
$router->map('GET', '/wallet/lp/{network}/{address}', 'App\Routes\Wallet::fetchLpIds');
$router->map('GET', '/wallet/lpvalues/{network}/{address}', 'App\Routes\Wallet::getUserValues');
$router->map('GET', '/walletcron/updatewallets', 'App\Routes\Wallet::updateAllUserValues');
$router->map('GET', '/wallet/farm/history/{address}', 'App\Routes\Wallet::getUserFarmHistory');
$router->map('GET', '/dex/populate', 'App\Routes\Dex::updateDex');
$router->map('GET', '/dex/build', 'App\Routes\Dex::buildRoute');
$router->map('GET', '/dobeefy', 'App\Routes\Front::dobeefy');
$router->map('GET', '/testaws', 'App\Routes\Front::testaws');
$router->map('GET', '/processqueue', 'App\Routes\Front::processqueue');
$router->map('GET', '/SymbolToLpRoute', 'App\Routes\SymbolToLpRoute::index');
$router->map('GET', '/buildBeethoven', 'App\Routes\LpBuildRoute::buildBeethoven');


try {
    $response = $router->dispatch($request);
} catch (\League\Route\Http\Exception\NotFoundException $ex)   {

    $fr = $container->get(Front::class);
    $response = $fr->index($request);

}
(new Zend\Diactoros\Response\SapiEmitter)->emit($response);



die;





die;
/*
$lpclient = new \App\Eth\Pools\PancakeLp('https://bsc-dataseed.binance.org/', $client);
$lpclient->getUsdValue();
die;*/

$poolInfo = new PancakeRouter('https://bsc-dataseed.binance.org/', $client);;
$poolInfo->getCoinPrice();
die;



$poolInfo = new PancakePool('https://bsc-dataseed.binance.org/', $client);;
$poolInfo->getAllPools();
die;
/*
//$pricesProvider = new \App\Eth\Price\cmcProvider($client);

//$pricesProvider->execute();

die;*/
$x = new EthClient('https://bsc-dataseed.binance.org/', $client);

//$t = $x->getContract('ahahal');
$t = $x->getTotal('ahahal');
print 'RESULTSSSSSS<br /><br /><br /><br /><br />';
var_dump($t); die;

die;
require_once("src/bootstrap.php");

session_start();
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER, $_GET, $_POST, $_COOKIE, $_FILES
);

$strategy = (new League\Route\Strategy\ApplicationStrategy)->setContainer($container);

/** @var \League\Route\Router $router */
$router = (new League\Route\Router)->setStrategy($strategy);

$router->map('GET', '/', 'Routes\Front::index');


$router->map('GET', '/login', 'Routes\Login::login');
$router->map('POST', '/doLogin', 'Routes\Login::doLogin');

try {
    $response = $router->dispatch($request);
} catch (\League\Route\Http\Exception\NotFoundException $ex)   {

    $fr = $container->get(Routes\Front::class);
    $response = $fr->index($request);

}
(new Zend\Diactoros\Response\SapiEmitter)->emit($response);
