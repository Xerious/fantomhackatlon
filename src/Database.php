<?php


namespace App;


class Database extends \PDO
{
    private $rnd;
    public function __construct()
    {
        parent::__construct("mysql:dbname=mltester;host=mysql", "root", "password");
        $this->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING );
    }
}