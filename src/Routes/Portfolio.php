<?php


namespace App\Routes;

use App\Models\UserAddresses;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Zend\Diactoros\Response\JsonResponse;

class Portfolio
{
    private EntityManager $connection;
    private Client $client;
    private Environment $twig;
    private Session $session;

    public function __construct(EntityManager $entityManager, Client $client, Environment $twig, Session $session)
    {
        $this->connection = $entityManager;
        $this->client = $client;
        $this->twig = $twig;
        $this->session = $session;
    }

    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $response = new \Zend\Diactoros\Response;

        $userAddressRepository = $this->connection->getRepository(UserAddresses::class);
        $addresses = $userAddressRepository->findBy([
            'user' => $this->session->get('user')
        ]);

        $res = $this->twig->render('pages/portfolio.twig', [
            'addresses' => $addresses
        ]);
        $response->getBody()->write($res);

        return $response;
    }

}