<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once "vendor/autoload.php";

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/src/Models",), $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

// or if you prefer yaml or XML
//$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);
//$config = Setup::createYAMLMetadataConfiguration(array(__DIR__."/config/yaml"), $isDevMode);

// database configuration parameters
if (file_exists('/var/www/instantlandpages/isamazonlive.dt')) {
    $conn = array(
        'dbname' => 'builder',
        'user' => 'ilpadmin',
        'password' => '789SuperFilth!',
        'host' => 'ilprds.c9hgg58coes0.us-east-1.rds.amazonaws.com',
        'driver' => 'pdo_mysql',
    );
} elseif (file_exists('/var/www/instantlandpages/isamazon.dt')) {
    $conn = array(
        'dbname' => 'builder',
        'user' => 'builder',
        'password' => '789SuperBuilder!',
        'host' => 'localhost',
        'driver' => 'pdo_mysql',
    );
} else  {
    $conn = array(
        'dbname' => 'yieldmonitor',
        'user' => 'root',
        'password' => 'password',
        'host' => 'mysql',
        'driver' => 'pdo_mysql',
    );
}
// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);
$conn1 = $entityManager->getConnection();
$conn1->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');