<?php

namespace App;

use League\Container\Container;
use League\Container\ContainerAwareTrait;
use League\Container\ContainerInterface;

class Application
{
    use ContainerAwareTrait;

    /**
     * @var Application
     */
    static $instance;
    static $cont;
    /**
     * @param ContainerInterface $container
     */
    public function __construct(Container $container)
    {
        static::setInstance($this);
       // $this->setContainer($container);

        $this->cont = $container;
        $this->registerErrorHandler();
    }

    public function getContainer(): \Psr\Container\ContainerInterface
    {
        return $this->cont;
    }

    /**
     * @param Application $instance
     */
    public static function setInstance(Application $instance)
    {
        static::$instance = $instance;
    }

    /**
     * @return Application
     */
    public static function getInstance()
    {
        return static::$instance;
    }

    /**
     * @return bool
     */
    public function isDownForMaintenance()
    {
        return file_exists(PATH_ROOT.'/storage/down');
    }

    /**
     * @return bool
     */
    public function isRunningInConsole()
    {
        return php_sapi_name() == 'cli' || php_sapi_name() == 'phpdbg';
    }

    /**
     * @return bool
     */
    public function isConsoleApplication()
    {
        return basename($_SERVER["SCRIPT_FILENAME"], '.php') === 'console';
    }

    /**
     * @return void
     */
    private function registerErrorHandler()
    {
      /*  $handlerContainerKey = $this->isRunningInConsole() ? 'error.handler.cli' : 'error.handler.http';

        /** @var Run $whoops */
       // $whoops = $this->container->get($handlerContainerKey);

       // $whoops->register();

        // We overwrite the whoops error handler, since it converts non fatal errors (warning/notices/...) to fatal
        // errors.
     //   set_error_handler([$this, 'handleError']);*/
    }

    /**
     * @return bool
     */
    public function handleError()
    {
        // Do nothing when a non fatal error occurs.

        return false;
    }

    /**
     * @param array $stages
     *
     * @return bool
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function isEnvironment(array $stages)
    {
        $environment = $this->container->get('config')->get('env');

        return in_array($environment, $stages);
    }
}
