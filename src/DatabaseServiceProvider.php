<?php

namespace App;

use League\Container\ServiceProvider\AbstractServiceProvider;
use PDO;

class DatabaseServiceProvider extends AbstractServiceProvider
{
    /**
     * @var array
     */
    protected $provides = [
       /* 'db.connections.mysql_01',*/
        'Database',
      /*  'Doctrine\DBAL\Logging\DebugStack',
        'Doctrine\DBAL\Logging\SQLLogger',
        'Doctrine\DBAL\Connection',*/
    ];

    protected $config = [];

    /**
     * DatabaseServiceProvider constructor.
     * @param array $provides
     */
    public function __construct()
    {
        $this->config['Database'] = [
            "dbname" =>  "dbname",
            "host" =>  "localhost",
            "user" =>  "finley",
            "password" =>  "password",
        ];
    }


    public function register()
    {
        /** @var Config $config */
        $config = ["mysql_01"];

        foreach ($config['database']['connections'] as $name => $connectionConfig) {

            $this->getContainer()->share('db.connections.' . $name, function () use ($connectionConfig, $config, $name) {
              /*  $connection = DriverManager::getConnection([
                    'dbname' => $this->config[$name]['database'],
                    'host' => $this->config[$name]['host'],
                    'user' => $this->config[$name]['username'],
                    'password' => $this->config[$name]['password'],
                    'driver' => 'pdo_mysql',
                ]);
                return $connection;*/

                $pdo = new PDO($this->config[$name]['host'], $this->config[$name]['username'], $this->config[$name]['password']);
                return $pdo;
                //return new Database($connection);
            });
        }

        $this->getContainer()->share('mycon', function () {
            return $this->getContainer()->get('db.connections.mysql_01');
        });

    }
}
