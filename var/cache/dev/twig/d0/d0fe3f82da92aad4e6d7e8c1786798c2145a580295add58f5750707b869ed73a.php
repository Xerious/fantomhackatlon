<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* component/chart.twig */
class __TwigTemplate_e47bfe7c2fe0244baa91557c0b3b16014ee17faf76e3aaaeae460c354c25368b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "component/chart.twig"));

        // line 29
        echo "
<div class=\"cardTop\">
    <div class=\"networkInfo graphPadding\">
        <div style=\"float: left;\">
            <div class=\"\">
                <div v-if=\"lpCoin.network == 'bsc'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/bsc.png\" class=\"networkBodyLogo\" /> Bsc</div>
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'fantom'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/fantom.png\" class=\"networkBodyLogo\" />Ftm</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'avaxc'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/avalanche.png\" class=\"networkBodyLogo\" />Avax</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'poly'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/polygon.png\" class=\"networkBodyLogo\" />Poly</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'mainnet'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/ethereum.png\" class=\"networkBodyLogo\" />Eth</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
            </div>
        </div>
        <div style=\"float: right; margin-top: 18px;\">
        ";
        if ((isset($context["wallet"]) || array_key_exists("wallet", $context) ? $context["wallet"] : (function () { throw new RuntimeError('Variable "wallet" does not exist.', 29, $this->source); })())) {
            // line 47
            echo "
              <span class=\"chartValue jostHead5 positive\" v-if=\"lastPercentageForLp[lpCoin.id] >= 0 \">
               \$ {{lastTotalForLp[lpCoin.id]}}
            </span>
              <span class=\"chartValue jostHead5 positive\" v-if=\"lastPercentageForLp[lpCoin.id] == 'tbd' \">
               \$ TBD
            </span>
            <span class=\"chartValue jostHead5\" v-if=\"lastPercentageForLp[lpCoin.id] < 0 \">
               \$ {{lastTotalForLp[lpCoin.id]}}
            </span>
            <span v-if=\"lastPercentageForLp[lpCoin.id] >= 0 \" class=\"positive resultFloat chartResult jostSub2\">
               <span class=\"material-icons\">trending_up</span>
                {{lastPercentageForLp[lpCoin.id]}}%
            </span>
            <span v-if=\"lastPercentageForLp[lpCoin.id] < 0 \"  class=\"negative resultFloat chartResult jostSub2\">
            <span class=\"material-icons\">trending_down</span>
                {{lastPercentageForLp[lpCoin.id]}}%
            </span>
        ";
        } else {
            // line 62
            echo "
              <span class=\"chartValue jostHead5 positive\" v-if=\"getCurrenPctResult(lpCoin) >= 0 \">
               \$ {{getCurrentDisplayPrice(lpCoin)}}
            </span>
            <span class=\"chartValue jostHead5\" v-if=\"getCurrenPctResult(lpCoin) < 0 \">
               \$ {{getCurrentDisplayPrice(lpCoin)}}
            </span>
            <span v-if=\"getCurrenPctResult(lpCoin) >= 0 \" class=\"positive resultFloat chartResult jostSub2\">
               <span class=\"material-icons\">trending_up</span>
                {{getCurrenPctResult(lpCoin)}}%
            </span>
            <span v-if=\"getCurrenPctResult(lpCoin) < 0 \"  class=\"negative resultFloat chartResult jostSub2\">
            <span class=\"material-icons\">trending_down</span>
                {{getCurrenPctResult(lpCoin)}}%
            </span>
        ";
        }
        // line 75
        echo "
        </div>

        <div style=\"clear: both\"></div>
    </div>
    <div class=\"symbolsText jostHead6\" v-if=\"lpCoin.symbol1Name != ''\">
        {{lpCoin.symbol0Name}} - {{lpCoin.symbol1Name}}
    </div>
    <div class=\"symbolsText jostHead6\" v-if=\"lpCoin.symbol1Name == ''\">
        {{lpCoin.symbol0Name}}
    </div>
</div>

";
        echo "
    ";
        // line 76
        if ((isset($context["wallet"]) || array_key_exists("wallet", $context) ? $context["wallet"] : (function () { throw new RuntimeError('Variable "wallet" does not exist.', 76, $this->source); })())) {
            // line 77
            echo "    <div class=\"graphHolderInner graphPadding\">
        <tradeview :wallet=\"";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["wallet"]) || array_key_exists("wallet", $context) ? $context["wallet"] : (function () { throw new RuntimeError('Variable "wallet" does not exist.', 78, $this->source); })()), "html", null, true);
            echo "\" :containerid=\"'chart'+lpCoin.id\" :pricedata=\"getdatatest(lpCoin.price)\" :userlpdata=\"getLpData(lpCoin)\" :userpricedata=\"getLpPriceData(lpCoin)\" :width=\"462\"></tradeview>
    </div>
    <div class=\"chart-actions-bar jostHead6 text-left\">
        <span class=\"waves-effect waves-light btn\">Trade Now </span><span class=\"comming-soon jostHead4\">Coming Soon</span>
        <div class=\"chart-actions-right\">
            <a :href=\"'/lp/detail/' +lpCoin.id\" target=\"_blank\">
                <span class=\"material-icons-outlined material-icons\">launch</span>
            </a>
        </div>
    </div>
    <div class=\"symbolsText jostHead6 text-left\">
        Farms
    </div>
        ";
            // line 98
            echo "
        <div class=\"chart-farm-holder\">
            <div v-for=\"farm in lpCoin.farms\" class=\"row text-left chart-farm-row jostHead4 pad8\">
                <div class=\"farm-chart-name\">{{farm.name}}</div> <div class=\"farm-chart-value text-right\"><span v-if=\"farmData[farm.id] && notExpiredToOld(farmData[farm.id])\">{{farmData[farm.id][0].value}}</span></div>
            </div>
        </div>
        <div class=\"pad8\"></div>
        ";
            echo "
    ";
        } else {
            // line 100
            echo "<div class=\"graphHolderInner graphPadding\">
        <tradeview :containerid=\"'chart'+lpCoin.id\" :pricedata=\"getdatatest(lpCoin.price)\"></tradeview>
</div>
        <div class=\"chart-actions-bar jostHead6 text-left\">
            <span class=\"waves-effect waves-light btn\">Trade Now  </span><span class=\"comming-soon jostHead4\">Coming Soon</span>
            <div class=\"chart-actions-right\">
                <a :href=\"'/lp/detail/' +lpCoin.id\" target=\"_blank\">
                    <span class=\"material-icons-outlined material-icons\">launch</span>
                </a>
            </div>
        </div>
    ";
        }
        // line 131
        echo "

<div style=\"display: none\">

    <a :href=\"'/lp/detail/' +lpCoin.id\">
                                    <span class=\"material-icons\" style=\"float: right;\">
                                    outbound
                                    </span>
    </a>
    <span v-if=\"!favorites.includes(lpCoin.id)\" v-on:click=\"addToFavorite(lpCoin.id)\"
          class=\"material-icons\"
          style=\"float: right;  cursor: pointer;\">
                                            favorite_border
                                     </span>
    <span v-if=\"favorites.includes(lpCoin.id)\" class=\"material-icons\"
          style=\"float: right; color: darkred;\">
                                    favorite
                                    </span>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "component/chart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 131,  160 => 100,  148 => 98,  132 => 78,  129 => 77,  127 => 76,  110 => 75,  92 => 62,  71 => 47,  40 => 29,);
    }

    public function getSourceContext()
    {
        return new Source("{% verbatim %}
<div class=\"cardTop\">
    <div class=\"networkInfo graphPadding\">
        <div style=\"float: left;\">
            <div class=\"\">
                <div v-if=\"lpCoin.network == 'bsc'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/bsc.png\" class=\"networkBodyLogo\" /> Bsc</div>
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'fantom'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/fantom.png\" class=\"networkBodyLogo\" />Ftm</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'avaxc'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/avalanche.png\" class=\"networkBodyLogo\" />Avax</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'poly'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/polygon.png\" class=\"networkBodyLogo\" />Poly</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
                <div v-if=\"lpCoin.network == 'mainnet'\">
                    <div class=\"jostHead4\"><img src=\"/public/networks/ethereum.png\" class=\"networkBodyLogo\" />Eth</div><br />
                    <div  v-if=\"lpCoin.symbol1Name != ''\" class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+lpCoin.techname+'.png'\" class=\"networkBodyLogo\" /> {{lpCoin.origin}}</div>
                </div>
            </div>
        </div>
        <div style=\"float: right; margin-top: 18px;\">
        {% endverbatim %}{% if wallet %}{% verbatim %}
              <span class=\"chartValue jostHead5 positive\" v-if=\"lastPercentageForLp[lpCoin.id] >= 0 \">
               \$ {{lastTotalForLp[lpCoin.id]}}
            </span>
              <span class=\"chartValue jostHead5 positive\" v-if=\"lastPercentageForLp[lpCoin.id] == 'tbd' \">
               \$ TBD
            </span>
            <span class=\"chartValue jostHead5\" v-if=\"lastPercentageForLp[lpCoin.id] < 0 \">
               \$ {{lastTotalForLp[lpCoin.id]}}
            </span>
            <span v-if=\"lastPercentageForLp[lpCoin.id] >= 0 \" class=\"positive resultFloat chartResult jostSub2\">
               <span class=\"material-icons\">trending_up</span>
                {{lastPercentageForLp[lpCoin.id]}}%
            </span>
            <span v-if=\"lastPercentageForLp[lpCoin.id] < 0 \"  class=\"negative resultFloat chartResult jostSub2\">
            <span class=\"material-icons\">trending_down</span>
                {{lastPercentageForLp[lpCoin.id]}}%
            </span>
        {% endverbatim %}{% else %}{% verbatim %}
              <span class=\"chartValue jostHead5 positive\" v-if=\"getCurrenPctResult(lpCoin) >= 0 \">
               \$ {{getCurrentDisplayPrice(lpCoin)}}
            </span>
            <span class=\"chartValue jostHead5\" v-if=\"getCurrenPctResult(lpCoin) < 0 \">
               \$ {{getCurrentDisplayPrice(lpCoin)}}
            </span>
            <span v-if=\"getCurrenPctResult(lpCoin) >= 0 \" class=\"positive resultFloat chartResult jostSub2\">
               <span class=\"material-icons\">trending_up</span>
                {{getCurrenPctResult(lpCoin)}}%
            </span>
            <span v-if=\"getCurrenPctResult(lpCoin) < 0 \"  class=\"negative resultFloat chartResult jostSub2\">
            <span class=\"material-icons\">trending_down</span>
                {{getCurrenPctResult(lpCoin)}}%
            </span>
        {% endverbatim %}{% endif %}{% verbatim %}
        </div>

        <div style=\"clear: both\"></div>
    </div>
    <div class=\"symbolsText jostHead6\" v-if=\"lpCoin.symbol1Name != ''\">
        {{lpCoin.symbol0Name}} - {{lpCoin.symbol1Name}}
    </div>
    <div class=\"symbolsText jostHead6\" v-if=\"lpCoin.symbol1Name == ''\">
        {{lpCoin.symbol0Name}}
    </div>
</div>

{% endverbatim %}
    {% if wallet %}
    <div class=\"graphHolderInner graphPadding\">
        <tradeview :wallet=\"{{wallet}}\" :containerid=\"'chart'+lpCoin.id\" :pricedata=\"getdatatest(lpCoin.price)\" :userlpdata=\"getLpData(lpCoin)\" :userpricedata=\"getLpPriceData(lpCoin)\" :width=\"462\"></tradeview>
    </div>
    <div class=\"chart-actions-bar jostHead6 text-left\">
        <span class=\"waves-effect waves-light btn\">Trade Now </span><span class=\"comming-soon jostHead4\">Coming Soon</span>
        <div class=\"chart-actions-right\">
            <a :href=\"'/lp/detail/' +lpCoin.id\" target=\"_blank\">
                <span class=\"material-icons-outlined material-icons\">launch</span>
            </a>
        </div>
    </div>
    <div class=\"symbolsText jostHead6 text-left\">
        Farms
    </div>
        {% verbatim %}
        <div class=\"chart-farm-holder\">
            <div v-for=\"farm in lpCoin.farms\" class=\"row text-left chart-farm-row jostHead4 pad8\">
                <div class=\"farm-chart-name\">{{farm.name}}</div> <div class=\"farm-chart-value text-right\"><span v-if=\"farmData[farm.id] && notExpiredToOld(farmData[farm.id])\">{{farmData[farm.id][0].value}}</span></div>
            </div>
        </div>
        <div class=\"pad8\"></div>
        {% endverbatim %}
    {% else %}
<div class=\"graphHolderInner graphPadding\">
        <tradeview :containerid=\"'chart'+lpCoin.id\" :pricedata=\"getdatatest(lpCoin.price)\"></tradeview>
</div>
        <div class=\"chart-actions-bar jostHead6 text-left\">
            <span class=\"waves-effect waves-light btn\">Trade Now  </span><span class=\"comming-soon jostHead4\">Coming Soon</span>
            <div class=\"chart-actions-right\">
                <a :href=\"'/lp/detail/' +lpCoin.id\" target=\"_blank\">
                    <span class=\"material-icons-outlined material-icons\">launch</span>
                </a>
            </div>
        </div>
    {% endif %}
{% verbatim %}

<div style=\"display: none\">

    <a :href=\"'/lp/detail/' +lpCoin.id\">
                                    <span class=\"material-icons\" style=\"float: right;\">
                                    outbound
                                    </span>
    </a>
    <span v-if=\"!favorites.includes(lpCoin.id)\" v-on:click=\"addToFavorite(lpCoin.id)\"
          class=\"material-icons\"
          style=\"float: right;  cursor: pointer;\">
                                            favorite_border
                                     </span>
    <span v-if=\"favorites.includes(lpCoin.id)\" class=\"material-icons\"
          style=\"float: right; color: darkred;\">
                                    favorite
                                    </span>
</div>
{% endverbatim %}", "component/chart.twig", "/var/www/default/templates/component/chart.twig");
    }
}
