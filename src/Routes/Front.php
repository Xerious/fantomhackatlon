<?php


namespace App\Routes;


use App\Models\User;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Zend\Diactoros\Response\RedirectResponse;

class Front
{
    private EntityManager $connection;
    private Client $client;
    private Environment $twig;
    private Session $session;

    public function __construct(EntityManager $entityManager, Client $client, Environment $twig, Session $session)
    {
        $this->connection = $entityManager;
        $this->client = $client;
        $this->twig = $twig;
        $this->session = $session;
    }

    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $response = new \Zend\Diactoros\Response;

        $res = $this->twig->render('pages/login.twig', [

        ]);
        $response->getBody()->write($res);

        return $response;
    }

    public function create(ServerRequestInterface $request): ResponseInterface
    {
        $response = new \Zend\Diactoros\Response;

        $res = $this->twig->render('pages/create.twig', [

        ]);
        $response->getBody()->write($res);

        return $response;
    }

    public function register(ServerRequestInterface $request): ResponseInterface {
        $email = $request->getParsedBody()['email'];
        $pwd = $request->getParsedBody()['password'];
        $pwd2 = $request->getParsedBody()['password2'];

        if ($pwd !== $pwd2) {
            return $this->failCreate('Passwords do not match');
        }

        if (strlen($pwd) < 5) {
            return $this->failCreate('Password to short');
        }

        $user = new User();
        $user->setEmail($email);
        $user->setPassw(md5($pwd));

        $this->connection->persist($user);
        $this->connection->flush();
        $this->session->set('user', $user);

        return new RedirectResponse('/user/addresses');
    }

    private function failCreate($message) {
        $response = new \Zend\Diactoros\Response;
        $res = $this->twig->render('pages/create.twig', [
            'error' => $message
        ]);
        $response->getBody()->write($res);

        return $response;
    }

    public function login(ServerRequestInterface $request): ResponseInterface {
        $email = $request->getParsedBody()['email'];
        $pwd = $request->getParsedBody()['password'];
        $userRepo = $this->connection->getRepository(User::class);
        $user = $userRepo->findOneBy([
            'email' => $email,
            'passw' => md5($pwd),
        ]);
        if ($user === null) {
            return new RedirectResponse('/');
        }


        $this->session->set('user', $user);

        return new RedirectResponse('/user/addresses');
    }
}