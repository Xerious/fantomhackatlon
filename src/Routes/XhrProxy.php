<?php


namespace App\Routes;


use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;

class XhrProxy
{
    private EntityManager $connection;
    private Client $client;
    private Environment $twig;
    private Session $session;

    public function __construct(EntityManager $entityManager, Client $client, Environment $twig, Session $session)
    {
        $this->connection = $entityManager;
        $this->client = $client;
        $this->twig = $twig;
        $this->session = $session;
    }
    public function index(ServerRequestInterface $request): ResponseInterface
    {
        $response = new \Zend\Diactoros\Response;

        $parsedRequest = json_decode($request->getBody()->getContents(), true);

        $url = $parsedRequest['url'];
        $payload = $parsedRequest['payload'] ?? null;
        $method = $parsedRequest['method'];

        if ($method === 'get') {
            $xhrResponse = $this->client->get($url)->getBody()->getContents();

            $response->getBody()->write($xhrResponse);
        }
        if ($method === 'post') {
            $options = [
                'headers'=> ['Content-Type' =>'application/json'],
                'body' => json_encode($payload)
            ];
            $xhrResponse = $this->client->post($url, $options)->getBody()->getContents();

            $response->getBody()->write($xhrResponse);
        }

        return $response;
    }

}