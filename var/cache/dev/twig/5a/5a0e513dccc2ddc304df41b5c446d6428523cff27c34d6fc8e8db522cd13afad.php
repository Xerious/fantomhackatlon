<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* component/networkchart.twig */
class __TwigTemplate_ea672f0fb39ff44bbbe93fba9f7c97c95dd557cfdddaf466e6b313aa47aa35a6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "component/networkchart.twig"));

        // line 1
        echo "<div class=\"col l4  m6 s12 margin-bottom-16\">
    <div class=\"cardTop card-home-network network-home-";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 2, $this->source); })()), "html", null, true);
        echo "\">
        <div class=\"networkInfo padding16\">
            <div class=\"\">
                ";
        // line 5
        if (((isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 5, $this->source); })()) == "bsc")) {
            // line 6
            echo "                    ";
            $context["networkname"] = "Binance";
            // line 7
            echo "                    <div>
                        <img src=\"/public/networks/bsc.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">BSC</div>
                            <div class=\"jostSub2 margintop4\">Binance</div>
                        </div>
                    </div>
                ";
        }
        // line 15
        echo "                ";
        if (((isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 15, $this->source); })()) == "fantom")) {
            // line 16
            echo "                    ";
            $context["networkname"] = "Fantom";
            // line 17
            echo "                    <div>
                        <img src=\"/public/networks/fantom.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">FTM</div>
                            <div class=\"jostSub2 margintop4\">Fantom</div>
                        </div>
                    </div>
                ";
        }
        // line 25
        echo "                ";
        if (((isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 25, $this->source); })()) == "avaxc")) {
            // line 26
            echo "                    ";
            $context["networkname"] = "Avalanche";
            // line 27
            echo "                    <div>
                        <img src=\"/public/networks/avalanche.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">AVAX</div>
                            <div class=\"jostSub2 margintop4\">Avalanche</div>
                        </div>
                    </div>
                ";
        }
        // line 35
        echo "                ";
        if (((isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 35, $this->source); })()) == "poly")) {
            // line 36
            echo "                    ";
            $context["networkname"] = "Polygon";
            // line 37
            echo "                    <div>
                        <img src=\"/public/networks/polygon.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">POLY</div>
                            <div class=\"jostSub2 margintop4\">Polygon</div>
                        </div>
                    </div>
                ";
        }
        // line 45
        echo "                ";
        if (((isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 45, $this->source); })()) == "mainnet")) {
            // line 46
            echo "                    ";
            $context["networkname"] = "Ethereum";
            // line 47
            echo "                    <div>
                        <img src=\"/public/networks/ethereum.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">ETH</div>
                            <div class=\"jostSub2 margintop4\">Ethereum</div>
                        </div>
                    </div>
                ";
        }
        // line 55
        echo "            </div>
        </div>
        <div class=\"border-home text-left\">
            <div class=\"padding16\" style=\"width: 50%; display: inline-block\">
                <span class=\"jostSub2\">LPs</span><br/>
                <span class=\"jostHead3\">";
        // line 60
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["totals"]) || array_key_exists("totals", $context) ? $context["totals"] : (function () { throw new RuntimeError('Variable "totals" does not exist.', 60, $this->source); })()), (isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 60, $this->source); })()), [], "array", false, false, false, 60), "lps", [], "any", false, false, false, 60), "html", null, true);
        echo "</span>
            </div>
            <div class=\"padding16 border-left\" style=\"width: 49%; display: inline-block\">
                <span class=\"jostSub2\">Farms</span><br/>
                <span class=\"jostHead3\">";
        // line 64
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["totals"]) || array_key_exists("totals", $context) ? $context["totals"] : (function () { throw new RuntimeError('Variable "totals" does not exist.', 64, $this->source); })()), (isset($context["network"]) || array_key_exists("network", $context) ? $context["network"] : (function () { throw new RuntimeError('Variable "network" does not exist.', 64, $this->source); })()), [], "array", false, false, false, 64), "farms", [], "any", false, false, false, 64), "html", null, true);
        echo "</span>
            </div>
        </div>
        <div>
            <div class=\"padding16 jostHead4\">
            VIEW ";
        // line 69
        echo twig_escape_filter($this->env, twig_upper_filter($this->env, (isset($context["networkname"]) || array_key_exists("networkname", $context) ? $context["networkname"] : (function () { throw new RuntimeError('Variable "networkname" does not exist.', 69, $this->source); })())), "html", null, true);
        echo " DEFI <span class=\"material-icons right-align\" style=\"vertical-align: bottom; float: right\">arrow_right_alt</span>
            </div>
        </div>
    </div>
</div>";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "component/networkchart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 69,  142 => 64,  135 => 60,  128 => 55,  118 => 47,  115 => 46,  112 => 45,  102 => 37,  99 => 36,  96 => 35,  86 => 27,  83 => 26,  80 => 25,  70 => 17,  67 => 16,  64 => 15,  54 => 7,  51 => 6,  49 => 5,  43 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"col l4  m6 s12 margin-bottom-16\">
    <div class=\"cardTop card-home-network network-home-{{ network }}\">
        <div class=\"networkInfo padding16\">
            <div class=\"\">
                {% if network == 'bsc' %}
                    {% set networkname='Binance' %}
                    <div>
                        <img src=\"/public/networks/bsc.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">BSC</div>
                            <div class=\"jostSub2 margintop4\">Binance</div>
                        </div>
                    </div>
                {% endif %}
                {% if network == 'fantom' %}
                    {% set networkname='Fantom' %}
                    <div>
                        <img src=\"/public/networks/fantom.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">FTM</div>
                            <div class=\"jostSub2 margintop4\">Fantom</div>
                        </div>
                    </div>
                {% endif %}
                {% if network == 'avaxc' %}
                    {% set networkname='Avalanche' %}
                    <div>
                        <img src=\"/public/networks/avalanche.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">AVAX</div>
                            <div class=\"jostSub2 margintop4\">Avalanche</div>
                        </div>
                    </div>
                {% endif %}
                {% if network == 'poly' %}
                    {% set networkname='Polygon' %}
                    <div>
                        <img src=\"/public/networks/polygon.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">POLY</div>
                            <div class=\"jostSub2 margintop4\">Polygon</div>
                        </div>
                    </div>
                {% endif %}
                {% if network == 'mainnet' %}
                    {% set networkname='Ethereum' %}
                    <div>
                        <img src=\"/public/networks/ethereum.png\" class=\"network-home-logo\"/>
                        <div class=\"home-chart-network-title\">
                            <div class=\"jostHead3\">ETH</div>
                            <div class=\"jostSub2 margintop4\">Ethereum</div>
                        </div>
                    </div>
                {% endif %}
            </div>
        </div>
        <div class=\"border-home text-left\">
            <div class=\"padding16\" style=\"width: 50%; display: inline-block\">
                <span class=\"jostSub2\">LPs</span><br/>
                <span class=\"jostHead3\">{{ totals[network].lps }}</span>
            </div>
            <div class=\"padding16 border-left\" style=\"width: 49%; display: inline-block\">
                <span class=\"jostSub2\">Farms</span><br/>
                <span class=\"jostHead3\">{{ totals[network].farms }}</span>
            </div>
        </div>
        <div>
            <div class=\"padding16 jostHead4\">
            VIEW {{ networkname|upper }} DEFI <span class=\"material-icons right-align\" style=\"vertical-align: bottom; float: right\">arrow_right_alt</span>
            </div>
        </div>
    </div>
</div>", "component/networkchart.twig", "/var/www/default/templates/component/networkchart.twig");
    }
}
