<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/index.twig */
class __TwigTemplate_66660d7dd6fc8809a9a44f9cdeb04b5cbd31ab734d5e45e593408610087c0eb4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/index.twig"));

        // line 1
        $this->loadTemplate("layout/header.twig", "pages/index.twig", 1)->display(twig_array_merge($context, ["page" => "lpcharts"]));
        // line 58
        echo "



<div id=\"mydiv\">

    <div id='inner-flex-container'>
<div class=\"contentHeader\">

</div>

<div class=\"pageContent \">
    <div>
    <div>
        <div class=\"contentMenuContainer homecontainer\">
            <div class=\"row\">
                 <div class=\"input-field col s6 m3 default-ym-input\" style=\"text-align: center\">
                    <input class=\"\" placeholder=\"Search\" @change=\"doSearch(\$event)\"/>

                    </div>
                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"searchChanged(\$event)\">
                          <option value=\"\" disabled selected>Sort by</option>
                          <option value=\"lpresult\">LP result</option>
                          <option value=\"name\">Name</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeFilterNetwork(\$event)\"  >
                             <option value=\"all\" disabled>Filter by network</option>
                             <option value=\"all\">All networks</option>
                            <option value=\"bsc\">Binance Smart Chain</option>
                            <option value=\"poly\">Polygon</option>
                            <option value=\"avaxc\">Avalanche</option>
                            <option value=\"fantom\">Fantom</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeAmountPerPage(\$event)\" >
                        <option disabled selected>Results per page</option>
                             <option>5</option>
                            <option>10</option>
                            <option >20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"graphHolder\">
            <div v-if=\"sortedCoinData.length == 0\" class=\"jostHead1\"> Loading ...</div>
            <div v-for=\"lpCoin in sortedCoinData\" v-if=\"\" style=\"\" class=\"maingraph\">
            ";
        echo "
               ";
        // line 59
        $this->loadTemplate("component/chart.twig", "pages/index.twig", 59)->display(twig_array_merge($context, ["wallet" => false]));
        // line 60
        echo "            ";
        // line 91
        echo "
            </div>
        </div>

        <div v-if=\"sortedCoinData.length != 0\" >
          <ul class=\"pagination jostHead5\">
            <li class=\"waves-effect\" v-if=\"currentPage >= 1\" v-on:click=\"currentPage--\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>
            <li class=\"disabled\" v-if=\"currentPage < 1\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>

            <li v-for=\"n in 3\" v-if=\"currentPage+1-4+n > 0\" class=\"waves-effect\" v-on:click=\"currentPage=currentPage-4+n\"><a href=\"#!\">{{currentPage+1-4+n}}</a></li>

            <li class=\"active\"><a href=\"#!\">{{currentPage+1}}</a></li>

            <li  v-for=\"n in 3\" v-if=\"currentPage+1+n<getTotalPages+1\" v-on:click=\"currentPage=currentPage+n\" class=\"waves-effect\"><a href=\"#!\">{{currentPage+1+n}}</a></li>


            <li class=\"waves-effect\"  v-on:click=\"currentPage++\"><a href=\"#!\"><i class=\"material-icons\">chevron_right</i></a></li>
          </ul>
          </div>

        <br />
    </div>
    <div style=\"clear: both;\"></div>
<div class=\"hide-on-large-only mobile-filler\">&nbsp;</div>
</div>

</div>

</div>
</div>
<script>
    const defaultNetwork = '";
        echo twig_escape_filter($this->env, (isset($context["selectedNetwork"]) || array_key_exists("selectedNetwork", $context) ? $context["selectedNetwork"] : (function () { throw new RuntimeError('Variable "selectedNetwork" does not exist.', 91, $this->source); })()), "html", null, true);
        // line 298
        echo "';
</script>
<script>
    var app = new Vue({
        el: '#mydiv',
        data: {
            allCoinData: [],
            hideCoinValues: false,
            HighestFirst: false,
            limitNumber: 20,
            currentPage: 0,
            searchTerm: \"\",
            networkFilter: \"all\",
            favorites: [],
            defaultNetwork: defaultNetwork
        },
        components: {
            highcharts: HighchartsVue.Chart
        },
        computed: {
            sortedCoinData() {
                var sortable = [...this.allCoinData];

                if (this.networkFilter !== 'all') {
                    var tmpSortable = sortable
                        .filter((element) => (element.network === this.networkFilter));
                    sortable = tmpSortable;
                }

                if (!this.HighestFirst)
                    return sortable
                        .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                        .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);
//return sortable;
                sortable.sort(function (a, b) {
                    if (parseFloat(a.result) >= parseFloat(b.result)) return -1;
                    if (parseFloat(a.result) <= parseFloat(b.result)) return 1;
                    return 0;
                });



                // console.log(news);
                return sortable
                    .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                    .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

            },
            getTotalPages() {
                  var sortable = [...this.allCoinData];

                if (this.networkFilter !== 'all') {
                    var tmpSortable = sortable
                        .filter((element) => (element.network === this.networkFilter));
                    sortable = tmpSortable;
                }

                if (!this.HighestFirst)
                    return Math.ceil( sortable
                        .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                        .length / this.limitNumber);


                // console.log(news);
                return  Math.ceil(sortable
                    .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm))).length / this.limitNumber);

            },
        },
        methods: {
            searchChanged(event) {
                if (event.target.value == 'lpresult') {
                    this.HighestFirst=true;
                    return
                }
                this.HighestFirst=false;
                return;
            },
            getCurrentPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 10000) {
                    return temp.toFixed(0);
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },

            getCurrentDisplayPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 1000000) {
                    return (temp / 1000).toFixed(0) + 'K';
                }
                if (temp > 10000) {
                    return (temp / 1000).toFixed(2) + 'K';
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },
            getCurrenPctResult(coin) {
                const now = coin.price[coin.price.length - 1].value;
                const last = coin.price[0].value;
                if (now > last) {
                    var diff = now - last;
                    var gain = diff/now*100;
                } else {
                    var diff = last - now;
                    var gain = -(diff/last*100);
                }
                return gain.toFixed(2);
            },
            favoritesLoaded(data) {
                for (var favoritekey in data) {
                    this.favorites.push(data[favoritekey].coinLP.id);
                }
            },
            getdatatest(coin, title = 'Price per share', ytext = 'USD') {
                return coin;
            },
            doSearch(event) {
                if (event.target.value.length === 0) {
                    this.searchTerm = '';
                    this.currentPage = 0;
                }

                if (event.target.value.length >= 3) {
                    this.searchTerm = event.target.value.toUpperCase();
                    this.currentPage = 0;
                }
            },
            changeAmountPerPage(event) {
                this.limitNumber = event.target.value;
                this.currentPage = 0;
            },
            changeFilterNetwork(event) {
                this.networkFilter = event.target.value;
                this.currentPage = 0;
            },
            addToFavorite(id) {
                if (!isConnected) {
                    alert('Please connect your wallet first');
                    // alert('Please connect your wallet first');
                } else {
                    axios.get('/lp/favorite/' + id + '/' + isConnected)
                        .then(response => {
                            this.favorites.push(id);
                        });
                }
            },

            fetchNextData(page) {
                let url;
                console.log(this.defaultNetwork);
                if (this.defaultNetwork !== null && this.defaultNetwork !== '') {
                     url = '/getpricesnetwork/poly';
                } else {
                    url = '/getprices';
                }
                axios.get(url + \"/\" + page)
                    .then(response => {
                        let amountReturned = 1;
                        for (var lpName in response.data) {
                            amountReturned++;
                            this.allCoinData.push(response.data[lpName]);
                        }

                        if (amountReturned > 10) {
                            this.fetchNextData(page + 1);
                        }
                    });
            }
        },
        mounted() {
             if (this.defaultNetwork !== null && this.defaultNetwork !== '') {
                 this.networkFilter = this.defaultNetwork;
            }
            setTimeout(this.fetchNextData(0) ,10);
        }
    })
</script>


";
        echo "

";
        // line 300
        $this->loadTemplate("layout/footer.twig", "pages/index.twig", 300)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "pages/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  352 => 300,  140 => 298,  106 => 91,  104 => 60,  102 => 59,  42 => 58,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'layout/header.twig'  with {'page': 'lpcharts' } %}
{% verbatim %}



<div id=\"mydiv\">

    <div id='inner-flex-container'>
<div class=\"contentHeader\">

</div>

<div class=\"pageContent \">
    <div>
    <div>
        <div class=\"contentMenuContainer homecontainer\">
            <div class=\"row\">
                 <div class=\"input-field col s6 m3 default-ym-input\" style=\"text-align: center\">
                    <input class=\"\" placeholder=\"Search\" @change=\"doSearch(\$event)\"/>

                    </div>
                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"searchChanged(\$event)\">
                          <option value=\"\" disabled selected>Sort by</option>
                          <option value=\"lpresult\">LP result</option>
                          <option value=\"name\">Name</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeFilterNetwork(\$event)\"  >
                             <option value=\"all\" disabled>Filter by network</option>
                             <option value=\"all\">All networks</option>
                            <option value=\"bsc\">Binance Smart Chain</option>
                            <option value=\"poly\">Polygon</option>
                            <option value=\"avaxc\">Avalanche</option>
                            <option value=\"fantom\">Fantom</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeAmountPerPage(\$event)\" >
                        <option disabled selected>Results per page</option>
                             <option>5</option>
                            <option>10</option>
                            <option >20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
            </div>
            <div style=\"clear: both\"></div>
        </div>
        <div class=\"graphHolder\">
            <div v-if=\"sortedCoinData.length == 0\" class=\"jostHead1\"> Loading ...</div>
            <div v-for=\"lpCoin in sortedCoinData\" v-if=\"\" style=\"\" class=\"maingraph\">
            {% endverbatim %}
               {% include 'component/chart.twig'  with {'wallet': false}  %}
            {% verbatim %}
            </div>
        </div>

        <div v-if=\"sortedCoinData.length != 0\" >
          <ul class=\"pagination jostHead5\">
            <li class=\"waves-effect\" v-if=\"currentPage >= 1\" v-on:click=\"currentPage--\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>
            <li class=\"disabled\" v-if=\"currentPage < 1\"><a href=\"#!\"><i class=\"material-icons\">chevron_left</i></a></li>

            <li v-for=\"n in 3\" v-if=\"currentPage+1-4+n > 0\" class=\"waves-effect\" v-on:click=\"currentPage=currentPage-4+n\"><a href=\"#!\">{{currentPage+1-4+n}}</a></li>

            <li class=\"active\"><a href=\"#!\">{{currentPage+1}}</a></li>

            <li  v-for=\"n in 3\" v-if=\"currentPage+1+n<getTotalPages+1\" v-on:click=\"currentPage=currentPage+n\" class=\"waves-effect\"><a href=\"#!\">{{currentPage+1+n}}</a></li>


            <li class=\"waves-effect\"  v-on:click=\"currentPage++\"><a href=\"#!\"><i class=\"material-icons\">chevron_right</i></a></li>
          </ul>
          </div>

        <br />
    </div>
    <div style=\"clear: both;\"></div>
<div class=\"hide-on-large-only mobile-filler\">&nbsp;</div>
</div>

</div>

</div>
</div>
<script>
    const defaultNetwork = '{% endverbatim %}{{selectedNetwork}}{% verbatim %}';
</script>
<script>
    var app = new Vue({
        el: '#mydiv',
        data: {
            allCoinData: [],
            hideCoinValues: false,
            HighestFirst: false,
            limitNumber: 20,
            currentPage: 0,
            searchTerm: \"\",
            networkFilter: \"all\",
            favorites: [],
            defaultNetwork: defaultNetwork
        },
        components: {
            highcharts: HighchartsVue.Chart
        },
        computed: {
            sortedCoinData() {
                var sortable = [...this.allCoinData];

                if (this.networkFilter !== 'all') {
                    var tmpSortable = sortable
                        .filter((element) => (element.network === this.networkFilter));
                    sortable = tmpSortable;
                }

                if (!this.HighestFirst)
                    return sortable
                        .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                        .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);
//return sortable;
                sortable.sort(function (a, b) {
                    if (parseFloat(a.result) >= parseFloat(b.result)) return -1;
                    if (parseFloat(a.result) <= parseFloat(b.result)) return 1;
                    return 0;
                });



                // console.log(news);
                return sortable
                    .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                    .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

            },
            getTotalPages() {
                  var sortable = [...this.allCoinData];

                if (this.networkFilter !== 'all') {
                    var tmpSortable = sortable
                        .filter((element) => (element.network === this.networkFilter));
                    sortable = tmpSortable;
                }

                if (!this.HighestFirst)
                    return Math.ceil( sortable
                        .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                        .length / this.limitNumber);


                // console.log(news);
                return  Math.ceil(sortable
                    .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm))).length / this.limitNumber);

            },
        },
        methods: {
            searchChanged(event) {
                if (event.target.value == 'lpresult') {
                    this.HighestFirst=true;
                    return
                }
                this.HighestFirst=false;
                return;
            },
            getCurrentPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 10000) {
                    return temp.toFixed(0);
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },

            getCurrentDisplayPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 1000000) {
                    return (temp / 1000).toFixed(0) + 'K';
                }
                if (temp > 10000) {
                    return (temp / 1000).toFixed(2) + 'K';
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },
            getCurrenPctResult(coin) {
                const now = coin.price[coin.price.length - 1].value;
                const last = coin.price[0].value;
                if (now > last) {
                    var diff = now - last;
                    var gain = diff/now*100;
                } else {
                    var diff = last - now;
                    var gain = -(diff/last*100);
                }
                return gain.toFixed(2);
            },
            favoritesLoaded(data) {
                for (var favoritekey in data) {
                    this.favorites.push(data[favoritekey].coinLP.id);
                }
            },
            getdatatest(coin, title = 'Price per share', ytext = 'USD') {
                return coin;
            },
            doSearch(event) {
                if (event.target.value.length === 0) {
                    this.searchTerm = '';
                    this.currentPage = 0;
                }

                if (event.target.value.length >= 3) {
                    this.searchTerm = event.target.value.toUpperCase();
                    this.currentPage = 0;
                }
            },
            changeAmountPerPage(event) {
                this.limitNumber = event.target.value;
                this.currentPage = 0;
            },
            changeFilterNetwork(event) {
                this.networkFilter = event.target.value;
                this.currentPage = 0;
            },
            addToFavorite(id) {
                if (!isConnected) {
                    alert('Please connect your wallet first');
                    // alert('Please connect your wallet first');
                } else {
                    axios.get('/lp/favorite/' + id + '/' + isConnected)
                        .then(response => {
                            this.favorites.push(id);
                        });
                }
            },

            fetchNextData(page) {
                let url;
                console.log(this.defaultNetwork);
                if (this.defaultNetwork !== null && this.defaultNetwork !== '') {
                     url = '/getpricesnetwork/poly';
                } else {
                    url = '/getprices';
                }
                axios.get(url + \"/\" + page)
                    .then(response => {
                        let amountReturned = 1;
                        for (var lpName in response.data) {
                            amountReturned++;
                            this.allCoinData.push(response.data[lpName]);
                        }

                        if (amountReturned > 10) {
                            this.fetchNextData(page + 1);
                        }
                    });
            }
        },
        mounted() {
             if (this.defaultNetwork !== null && this.defaultNetwork !== '') {
                 this.networkFilter = this.defaultNetwork;
            }
            setTimeout(this.fetchNextData(0) ,10);
        }
    })
</script>


{% endverbatim %}

{% include 'layout/footer.twig' %}", "pages/index.twig", "/var/www/default/templates/pages/index.twig");
    }
}
