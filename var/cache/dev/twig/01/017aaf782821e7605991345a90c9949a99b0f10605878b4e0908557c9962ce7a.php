<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/home.twig */
class __TwigTemplate_63fd8ebc7ab36687f824f8fc692906f567493547f3d13b8142196f14ea8980ee extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/home.twig"));

        // line 1
        $this->loadTemplate("layout/header.twig", "pages/home.twig", 1)->display(twig_array_merge($context, ["page" => "home"]));
        // line 2
        echo "
<div class=\"row ym-mobile-pad-top text-left \">
 <div class=\"padding16\">
 <span class=\"jostHead2 text-left \">Accurate, on-chain data makes DeFi accessible<br />
  and profitable for everyone.</span>
 </div>
</div>

<div class=\"row \">
 ";
        // line 11
        $this->loadTemplate("component/networkchart.twig", "pages/home.twig", 11)->display(twig_array_merge($context, ["network" => "avaxc"]));
        // line 12
        echo " ";
        $this->loadTemplate("component/networkchart.twig", "pages/home.twig", 12)->display(twig_array_merge($context, ["network" => "bsc"]));
        // line 13
        echo " ";
        $this->loadTemplate("component/networkchart.twig", "pages/home.twig", 13)->display(twig_array_merge($context, ["network" => "mainnet"]));
        // line 14
        echo " ";
        $this->loadTemplate("component/networkchart.twig", "pages/home.twig", 14)->display(twig_array_merge($context, ["network" => "fantom"]));
        // line 15
        echo " ";
        $this->loadTemplate("component/networkchart.twig", "pages/home.twig", 15)->display(twig_array_merge($context, ["network" => "poly"]));
        // line 16
        echo "
 <div class=\"col l4  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left\">
   <div class=\"margin-bottom-8\">
   <img src=\"/public/networks/bsc.png\" class=\"network-home-sub-logo\"/>
   <img src=\"/public/networks/fantom.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/avalanche.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/polygon.png\" class=\"network-home-sub-logo margin-left-8\"/>
   </div>
   <div class=\"margin-bottom-8\">
   <img src=\"/public/networks/ethereum.png\" class=\"network-home-sub-logo\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   </div>
   <div class=\"margin-bottom-16 \">
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
    <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   </div>
     <a href=\"/lpcharts\"><div style=\"margin-top: 28px\" class=\"jostHead4\">VIEW ALL DEFI <span class=\"material-icons right-align\" style=\"vertical-align: bottom; float: right\">arrow_right_alt</span></div></a>
  </div>
 </div>

</div>

<div class=\"row\">
  <div class=\"col l3  m6 s12 margin-bottom-16  \">
    <div class=\"padding16 home-inverse card-home-network text-left home-card-1\">
    <span class=\"jostHead3\"> How Yield Monitor’s database helps DeFi investors thrive.</span>
    </div>
  </div>
 <div class=\"col l3  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left home-card-2\">
   <span class=\"jostHead3\">Data gives information. Community is how we all thrive.</span>
  </div>
 </div>
 <div class=\"col l3  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left home-card-3\">
   <span class=\"jostHead3\">Are you a new crypto investor? Learn the foundations of DeFi.</span>
  </div>
 </div>
 <div class=\"col l3  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left home-card-4\">
   <span class=\"jostHead3\">View our plans for integrations in 2021 and 2022 features.</span>
  </div>
 </div>
</div>

<div class=\"hide-on-large-only mobile-filler\">&nbsp;</div>
";
        // line 67
        $this->loadTemplate("layout/footer.twig", "pages/home.twig", 67)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "pages/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 67,  67 => 16,  64 => 15,  61 => 14,  58 => 13,  55 => 12,  53 => 11,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'layout/header.twig' with {'page': 'home' } %}

<div class=\"row ym-mobile-pad-top text-left \">
 <div class=\"padding16\">
 <span class=\"jostHead2 text-left \">Accurate, on-chain data makes DeFi accessible<br />
  and profitable for everyone.</span>
 </div>
</div>

<div class=\"row \">
 {% include 'component/networkchart.twig'  with {'network': 'avaxc'}  %}
 {% include 'component/networkchart.twig'  with {'network': 'bsc'}  %}
 {% include 'component/networkchart.twig'  with {'network': 'mainnet'}  %}
 {% include 'component/networkchart.twig'  with {'network': 'fantom'}  %}
 {% include 'component/networkchart.twig'  with {'network': 'poly'}  %}

 <div class=\"col l4  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left\">
   <div class=\"margin-bottom-8\">
   <img src=\"/public/networks/bsc.png\" class=\"network-home-sub-logo\"/>
   <img src=\"/public/networks/fantom.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/avalanche.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/polygon.png\" class=\"network-home-sub-logo margin-left-8\"/>
   </div>
   <div class=\"margin-bottom-8\">
   <img src=\"/public/networks/ethereum.png\" class=\"network-home-sub-logo\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   </div>
   <div class=\"margin-bottom-16 \">
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
    <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   <img src=\"/public/networks/empty.png\" class=\"network-home-sub-logo margin-left-8\"/>
   </div>
     <a href=\"/lpcharts\"><div style=\"margin-top: 28px\" class=\"jostHead4\">VIEW ALL DEFI <span class=\"material-icons right-align\" style=\"vertical-align: bottom; float: right\">arrow_right_alt</span></div></a>
  </div>
 </div>

</div>

<div class=\"row\">
  <div class=\"col l3  m6 s12 margin-bottom-16  \">
    <div class=\"padding16 home-inverse card-home-network text-left home-card-1\">
    <span class=\"jostHead3\"> How Yield Monitor’s database helps DeFi investors thrive.</span>
    </div>
  </div>
 <div class=\"col l3  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left home-card-2\">
   <span class=\"jostHead3\">Data gives information. Community is how we all thrive.</span>
  </div>
 </div>
 <div class=\"col l3  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left home-card-3\">
   <span class=\"jostHead3\">Are you a new crypto investor? Learn the foundations of DeFi.</span>
  </div>
 </div>
 <div class=\"col l3  m6 s12 margin-bottom-16  \">
  <div class=\"padding16 home-inverse card-home-network text-left home-card-4\">
   <span class=\"jostHead3\">View our plans for integrations in 2021 and 2022 features.</span>
  </div>
 </div>
</div>

<div class=\"hide-on-large-only mobile-filler\">&nbsp;</div>
{% include 'layout/footer.twig' %}", "pages/home.twig", "/var/www/default/templates/pages/home.twig");
    }
}
