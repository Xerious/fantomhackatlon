<?php


namespace App\Models;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User implements \JsonSerializable
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $passw;

    /**
     * @ORM\Column(type="string")
     */
    protected $google;


    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return CoinFarm
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return CoinFarm
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassw()
    {
        return $this->passw;
    }

    /**
     * @param mixed $passw
     * @return CoinFarm
     */
    public function setPassw($passw)
    {
        $this->passw = $passw;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogle()
    {
        return $this->google;
    }

    /**
     * @param mixed $google
     * @return CoinFarm
     */
    public function setGoogle($google)
    {
        $this->google = $google;
        return $this;
    }



    /**
     * @return mixed|object
     */
    public function jsonSerialize()
    {


        $lol = get_object_vars($this);

        $lol['params'] = '';

        return (object)$lol;
    }
}
