Vue.component('yieldtext', {
    data: function () {
        return {
            number: 0,
            text: ""
        }
    },
    methods: {

    },
    mounted() {

    },
    template: '' +
        '<span v-if="number > 0" style="color: darkgreen;">{{text}}</span>' +
        '<span v-if="number < 0" style="color: darkred;">{{text}}</span>' +
        '<span v-if="number == 0" style="color: black;">{{text}}</span>' +
        ''
})