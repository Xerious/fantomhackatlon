<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/wallet.twig */
class __TwigTemplate_944c57c5f5fc2a5f0a4ac92edd0e58baef53ba99675c2e8a350d005087e360e0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/wallet.twig"));

        // line 1
        $this->loadTemplate("layout/header.twig", "pages/wallet.twig", 1)->display(twig_array_merge($context, ["page" => "wallet"]));
        // line 98
        echo "



<div id=\"mydiv\">

    <div id='inner-flex-container'>
<div class=\"contentHeader\">

</div>

<div class=\"pageContent walletPage\">
    <div>
       <div class=\"contentMenuContainer homecontainer\">


                <div v-if=\"walletId == ''\"> <br /><br /><h1></h1><br />
                <span class=\"jostHead2\">Enter your wallet below:*</span><br />

                 <div class=\"input-field col s6 m3 default-ym-input\" style=\"text-align: center\">
                    <input type=\"text\" placeholder=\"0x123abc456def789hijk\" v-model=\"walletinput\" class=\"yminputoverwrite\" /> &nbsp; <span class=\"waves-effect waves-light btn jostHead6\" @click=\"setWallet()\">View Wallet</span><br /><br />
                </div>

                <div class=\"jostSub2\">
                    <b>Note:</b> this feature is in public Alpha v1.1 - you are an early user.<br />
                    Please join Yield Monitor's discord and help provide feedback.<br />
                    <br />
                    Yield Monitor is accessing public blockchain data. We have no access to your funds. <br />
                    Your money is completely safe here. We are showing you your own data.  <br />
                </div>
            </div>
            <div v-if=\"sortedCoinData.length == 0 && walletId != '' && !noData\" class=\"jostHead3\" > Fetching your wallet data...<br />This may take upto 5 minutes the first time, please be patient!<br />Your token balances can take upto 30 minutes to show<br /></div>
            <div v-if=\"noData\" class=\"jostHead3\">Sorry, we currently cannot display your wallet, try again later.<br />If the problem persists there might not be anything to track.<br />Contact us in discord for more info or to get a farm added</div>
            <div class=\"row\"  :class=\"searchClass + ' jostHead3'\">Your personal page: https://app.yieldmonitor.io/wallet/{{walletId}}</div>
            <div class=\"row\"  :class=\"searchClass\">
                 <div class=\"input-field col s6 m3 default-ym-input\" style=\"text-align: center\">
                    <input class=\"\" placeholder=\"Search\" @change=\"doSearch(\$event)\"/>

                    </div>
                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select  @change=\"searchChanged(\$event)\">
                          <option value=\"\" disabled selected>Sort by</option>
                          <option value=\"lpresult\">LP result</option>
                          <option value=\"name\">Name</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeFilterNetwork(\$event)\"  >
                             <option value=\"all\" disabled>Filter by network</option>
                             <option value=\"all\">All networks</option>
                            <option value=\"bsc\">Binance Smart Chain</option>
                            <option value=\"poly\">Polygon</option>
                            <option value=\"avaxc\">Avalanche</option>
                            <option value=\"fantom\">Fantom</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeAmountPerPage(\$event)\" >
                        <option disabled selected>Results per page</option>
                             <option>5</option>
                            <option>10</option>
                            <option >20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
            </div>


            <div style=\"clear: both\"></div>



        </div>
        <div style=\"clear: both\"></div>
        <div class=\"graphHolder\"  v-if=\"sortedCoinData.length != 0\">
            <div v-if=\"sortedCoinData.length == 0\"> Loading ...</div>
             <div style=\"width: 100%;\" v-if=\"sortedCoinData.length != 0\">
                <div  class=\"maingraph padding16\" style=\"width: 660px; display: inline-block\">
                    <tradeview containerid=\"totals\" :pricedata=\"getTotalGraph()\" :width=620 :height=230></tradeview>
                </div>
                <div class=\"maingraph jostHead4\" style=\"width: 470px; /*max-width: 600px;*/ width: auto; display: inline-block; text-align: left\">
                    <div class=\"padding16\">
                        Thanks for being an Alpha v1.1 tester.<br />We appreciate you. <br />
                        <b>Please note:</b><br /><br />
                         Blue chart graphs should appear quickly, they represent the LP token price, as you are used to seeing on Yield Monitor already.<br />
                        <br />
                        User token and Price charts will take some time to appear (up to 15 minutes) during your first connect.<br />
                        This is because Yield Monitor is fetching all chain data in this initial connect.<br />
                    </div>
                </div>
            </div>
            <div v-for=\"lpCoin in sortedCoinData\" v-if=\"\" style=\"\" class=\"maingraph\">
            ";
        echo "
               ";
        // line 99
        $this->loadTemplate("component/chart.twig", "pages/wallet.twig", 99)->display(twig_array_merge($context, ["wallet" => true]));
        // line 100
        echo "            ";
        // line 133
        echo "
            </div>
        </div>

        <br />
    </div>
</div>

</div>

<div class=\"hide-on-large-only mobile-filler\"></div>
</div>
</div><script>

    //const rawData = '';

    //let decodedData = JSON.parse(rawData);

    var app = new Vue({
        el: '#mydiv',
        data: {
            allCoinData: [],
            hideCoinValues: false,
            HighestFirst: false,
            noData: false,
            limitNumber: 20,
            currentPage: 0,
            searchTerm: \"\",
            favorites: [],
            walletIds: [],
            farmData: {},
            networkFilter: \"all\",
            totalUsd: {},
            walletId: '";
        echo twig_escape_filter($this->env, (isset($context["walletAddress"]) || array_key_exists("walletAddress", $context) ? $context["walletAddress"] : (function () { throw new RuntimeError('Variable "walletAddress" does not exist.', 133, $this->source); })()), "html", null, true);
        // line 559
        echo "',
            walletinput: '',
            highestTimestamp : 0,
            lastTotalForLp: {},
            lastPercentageForLp: {},
            done:0,
            searchClass: 'hide',
        },
        watch: {
            lastTotalForLp: function (val) {

            },
        },
        components: {
            highcharts: HighchartsVue.Chart,
           /*tradeview: tradeview*/
        },
        computed: {
            sortedCoinData() {
                var sortable = [...this.allCoinData];
                this.totalUsd = {};

                 if (this.networkFilter !== 'all') {
                    var tmpSortable = sortable
                        .filter((element) => (element.network === this.networkFilter));
                    sortable = tmpSortable;
                }

                if (!this.HighestFirst)
                    return sortable
                        .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                        .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);
//return sortable;
                sortable.sort(function (a, b) {
                    if (parseFloat(a.result) >= parseFloat(b.result)) return -1;
                    if (parseFloat(a.result) <= parseFloat(b.result)) return 1;
                    return 0;
                });

                // console.log(news);
                return sortable
                    .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                    .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

            }
        },
        methods: {
            searchChanged(event) {
                if (event.target.value == 'lpresult') {
                    this.HighestFirst=true;
                    return
                }
                this.HighestFirst=false;
                return;
            },
            changeFilterNetwork(event) {
                this.networkFilter = event.target.value;
                this.currentPage = 0;
            },
             getCurrentDisplayPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 1000000) {
                    return (temp / 1000).toFixed(0) + 'K';
                }
                if (temp > 10000) {
                    return (temp / 1000).toFixed(2) + 'K';
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },
            getCurrentPrice (coin) {
                return this.lastTotalForLp[coin.id];
            },
            getCurrenPctResult (coin){
                return this.lastPercentageForLp[coin.id];
            },
            setWallet(){
                this.walletId = this.walletinput;
                this.loadWallet();
            },
            notExpiredToOld(coin) {
                var diff = this.highestTimestamp - coin[0].time;

                if (diff == 0) {
                    return true;
                }
                if (diff < 21000) {
                    return true;
                }
                return false;
            },
            loadWallet() {
                let walletAddress = this.walletId;
                axios.get('/wallet/lp/bsc/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        if (response.data['lps'] !== undefined)
                        this.walletIds = [...this.walletIds, ...response.data['lps']];
                       
                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });

                axios.get('/wallet/lp/avaxc/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        if (response.data['lps'] !== undefined)
                        this.walletIds = [...this.walletIds, ...response.data['lps']];
                       

                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });

                axios.get('/wallet/lp/fantom/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        if (response.data['lps'] !== undefined)
                        this.walletIds = [...this.walletIds, ...response.data['lps']];
                        
                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });

                axios.get('/wallet/farm/history/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        for(item in response.data) {
                            this.farmData[item] = response.data[item];
                        }
                        
                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });
            },
            getTotalGraph() {
                // find largest farm data
                    // run these values,
                        // loop each farm to add total.
                let largest = 0;
                let largestId = 0;
                let timeBasedArray = [];
                let resultArray = [];
                let countResults = {};

                for (farmId in this.farmData) {
                    if (this.farmData[farmId].length > largest) {
                        largest = this.farmData[farmId].length;
                        largestId = farmId;
                    }
                    timeBasedArray[farmId] = [];

                    for(dataKey in this.farmData[farmId]) {
                        timeBasedArray[farmId][this.farmData[farmId][dataKey].time] = this.farmData[farmId][dataKey];
                    }


                }

                for (arrayKey in this.farmData[largestId]) {
                    const currentTimestamp = this.farmData[largestId][arrayKey].time;
                    resultArray[currentTimestamp] = 0;

                    if (currentTimestamp > this.highestTimestamp) {
                        this.highestTimestamp = currentTimestamp;
                    }


                    for (farmId in this.farmData) {
                        if (timeBasedArray[farmId][currentTimestamp]) {
                            resultArray[currentTimestamp] += parseFloat(timeBasedArray[farmId][currentTimestamp].value) *  parseFloat(timeBasedArray[farmId][currentTimestamp].price);

                            if (countResults[currentTimestamp] === undefined) {
                                countResults[currentTimestamp] = 0;
                            }
                            if (parseFloat(timeBasedArray[farmId][currentTimestamp].value) > 0)
                                countResults[currentTimestamp] ++;

                        }
                    }

                }
//console.log(resultArray);

                let finalArray = [];
                let prevTime = 0;
                let prevItem = {};
                let item1Back = null;
                let item2Back = null;
                for(time in resultArray) {

                    // current
                    if (item2Back !== null) {
                       // console.log(countResults[item2Back.time], countResults[item1Back.time], countResults[time]);
                        if (countResults[item1Back.time] < countResults[time] && countResults[item1Back.time] < countResults[item2Back.time] ){
                        //    console.log('neinn', item1Back.time);
                        } else {
                            finalArray.push(item1Back); // push prev item
                        }
                    }


                    item2Back = item1Back;
                    item1Back = {'time': parseInt(time), 'value': resultArray[time]};

                   /* if (prevTime !== 0 && countResults[prevTime] > countResults[time]) {
                        prevItem = {};
                        continue;
                    }
                    if (prevItem !== {}) {
                        finalArray.push(prevItem);
                    }


                    prevItem = {'time': parseInt(time), 'value': resultArray[time]};*/
                }

                finalArray.push(item1Back);
              //  return [{'time': 0, 'value': 0}];
                return finalArray;
            },
            getCoinTotal(symbol) {
                var tot = 0.0;
                const price = symbol.price[symbol.price.length -1].value;

                for (farmk in symbol.farms) {
                    const farm = symbol.farms[farmk];
                    if (this.farmData[farm.id] !== undefined) {

                        tot += parseFloat(this.farmData[farm.id][0].value);
                        this.totalUsd[farm.id] = price * tot;
                    }
                }

                return (tot * price).toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                });
            },
            getTotalUsd() {
                let totalCounted = 0;

                for(lpId in this.allCoinData) {
                    const lp = this.allCoinData[lpId];
                    const price = parseFloat(lp.price[lp.price.length -1].value);

                    for (farmk in lp.farms) {
                        const farm = lp.farms[farmk];
                        if (this.farmData[farm.id] !== undefined) {
                            totalCounted += price * parseFloat(this.farmData[farm.id][0]['value']);
                        }
                    }
                }


                return totalCounted.toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                });
            },
            favoritesLoaded(data) {
           // isConnected = '0x78d4498970F099e3C94cA8e6Bd90C5795D076041';


                for (var favoritekey in data) {
                    this.favorites.push(data[favoritekey].coinLP.id);
                }
               // this.fetchFavoriteLpData();
            },
            doSearch(event) {
                if (event.target.value.length === 0) {
                    this.searchTerm = '';
                }

                if (event.target.value.length >= 3) {
                    this.searchTerm = event.target.value.toUpperCase();
                }
            },
            changeAmountPerPage(event) {
                this.currentPage = 0;
                this.limitNumber = event.target.value;
            },
            addToFavorite(id) {
                if (!isConnected) {
                    alert('Please connect your wallet first');
                   // alert('Please connect your wallet first');
                } else {
                    axios.get('/lp/favorite/'+id+'/'+isConnected)
                        .then(response =>  {
                            this.favorites.push(id);
                        });
                }
            },
            getdatatest(coin, title = 'Price per share', ytext = 'USD') {
                return coin;
            },
            getLpData(coin) {
                let resultArray = []
                for(farmKey in coin.farms) {
                   const farm = coin.farms[farmKey];
                    if (this.farmData[farm.id]) {
                        for(arrayKey in this.farmData[farm.id]) {
                            const cur = this.farmData[farm.id][arrayKey];
                            if (cur.value === '-1') {
                                continue;
                            }
                            if (resultArray[cur.time] === undefined) resultArray[cur.time] = 0;

                            resultArray[cur.time] += parseFloat(cur.value);
                        }
                    }
                }

                let finalArray = [];
                for(time in resultArray) {
                    finalArray.push({'time': parseInt(time), 'value': resultArray[time]});
                }

                return finalArray;
            },
            getLpPriceData(coin) {
                let resultArray = []
                for(farmKey in coin.farms) {
                   const farm = coin.farms[farmKey];
                    if (this.farmData[farm.id]) {
                        for(arrayKey in this.farmData[farm.id]) {
                            const cur = this.farmData[farm.id][arrayKey];
                            if (cur.value === '-1') {
                                continue;
                            }
                            if (resultArray[cur.time] === undefined) resultArray[cur.time] = 0;
                            resultArray[cur.time] += (parseFloat(cur.value) * parseFloat(cur.price));
                        }
                    }
                }

                let finalArray = [];
                for(time in resultArray) {
                    finalArray.push({'time': parseInt(time), 'value': resultArray[time]});
                }

                let shouldRefresh = false;
                if (this.lastTotalForLp[coin.id] === undefined) {
                    shouldRefresh = true;
                }
                if (finalArray.length > 2) {
                    this.lastTotalForLp[coin.id] = finalArray[finalArray.length - 1].value.toFixed(2);
                    var now =finalArray[finalArray.length - 1].value
                    var last =finalArray[0].value;
                    if (now > last) {
                        var diff = now - last;
                        var gain = diff/now*100;
                    } else {
                        var diff = last - now;
                        var gain = -(diff/last*100);
                    }

                    this.lastPercentageForLp[coin.id] = gain.toFixed(2);
                } else {
                    this.lastPercentageForLp[coin.id] = 'tbd';
                    shouldRefresh = false;
                }
                if (shouldRefresh) {
                    this.\$forceUpdate();
                }

                return finalArray;
            },
            fetchFavoriteLpData(page) {
                axios.post(\"/getpricesforlps\",{lps: this.walletIds, 'includeFarms': true, 'includeInactive': true})
                    .then(response =>  {
                        let amountReturned = 1;
                        this.allCoinData = [];
                        for (var lpName in response.data) {
                            amountReturned++;
                            this.allCoinData.push(response.data[lpName]);
                        }

                        if (amountReturned > 10) {
                          //  this.fetchNextData(page+1);
                        }

                        if (this.allCoinData.length === 0) {
                        this.noData = true;
                        return;
                        }

                        this.searchClass = 'show';
                    });
            }
        },
        mounted() {
           /* this.fetchNextData(0);*/

            if (this.walletId !== '') {
                this.loadWallet();
            }

        }
    });

</script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, {});
    });
</script>
";
        echo "

";
        // line 561
        $this->loadTemplate("layout/footer.twig", "pages/wallet.twig", 561)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "pages/wallet.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  613 => 561,  182 => 559,  146 => 133,  144 => 100,  142 => 99,  42 => 98,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'layout/header.twig' with {'page': 'wallet' }  %}
{% verbatim %}



<div id=\"mydiv\">

    <div id='inner-flex-container'>
<div class=\"contentHeader\">

</div>

<div class=\"pageContent walletPage\">
    <div>
       <div class=\"contentMenuContainer homecontainer\">


                <div v-if=\"walletId == ''\"> <br /><br /><h1></h1><br />
                <span class=\"jostHead2\">Enter your wallet below:*</span><br />

                 <div class=\"input-field col s6 m3 default-ym-input\" style=\"text-align: center\">
                    <input type=\"text\" placeholder=\"0x123abc456def789hijk\" v-model=\"walletinput\" class=\"yminputoverwrite\" /> &nbsp; <span class=\"waves-effect waves-light btn jostHead6\" @click=\"setWallet()\">View Wallet</span><br /><br />
                </div>

                <div class=\"jostSub2\">
                    <b>Note:</b> this feature is in public Alpha v1.1 - you are an early user.<br />
                    Please join Yield Monitor's discord and help provide feedback.<br />
                    <br />
                    Yield Monitor is accessing public blockchain data. We have no access to your funds. <br />
                    Your money is completely safe here. We are showing you your own data.  <br />
                </div>
            </div>
            <div v-if=\"sortedCoinData.length == 0 && walletId != '' && !noData\" class=\"jostHead3\" > Fetching your wallet data...<br />This may take upto 5 minutes the first time, please be patient!<br />Your token balances can take upto 30 minutes to show<br /></div>
            <div v-if=\"noData\" class=\"jostHead3\">Sorry, we currently cannot display your wallet, try again later.<br />If the problem persists there might not be anything to track.<br />Contact us in discord for more info or to get a farm added</div>
            <div class=\"row\"  :class=\"searchClass + ' jostHead3'\">Your personal page: https://app.yieldmonitor.io/wallet/{{walletId}}</div>
            <div class=\"row\"  :class=\"searchClass\">
                 <div class=\"input-field col s6 m3 default-ym-input\" style=\"text-align: center\">
                    <input class=\"\" placeholder=\"Search\" @change=\"doSearch(\$event)\"/>

                    </div>
                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select  @change=\"searchChanged(\$event)\">
                          <option value=\"\" disabled selected>Sort by</option>
                          <option value=\"lpresult\">LP result</option>
                          <option value=\"name\">Name</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeFilterNetwork(\$event)\"  >
                             <option value=\"all\" disabled>Filter by network</option>
                             <option value=\"all\">All networks</option>
                            <option value=\"bsc\">Binance Smart Chain</option>
                            <option value=\"poly\">Polygon</option>
                            <option value=\"avaxc\">Avalanche</option>
                            <option value=\"fantom\">Fantom</option>
                        </select>
                    </div>

                    <div class=\"input-field col s6 m3 default-ym-input\">
                        <select @change=\"changeAmountPerPage(\$event)\" >
                        <option disabled selected>Results per page</option>
                             <option>5</option>
                            <option>10</option>
                            <option >20</option>
                            <option>50</option>
                            <option>100</option>
                        </select>
                    </div>
                </div>
            </div>


            <div style=\"clear: both\"></div>



        </div>
        <div style=\"clear: both\"></div>
        <div class=\"graphHolder\"  v-if=\"sortedCoinData.length != 0\">
            <div v-if=\"sortedCoinData.length == 0\"> Loading ...</div>
             <div style=\"width: 100%;\" v-if=\"sortedCoinData.length != 0\">
                <div  class=\"maingraph padding16\" style=\"width: 660px; display: inline-block\">
                    <tradeview containerid=\"totals\" :pricedata=\"getTotalGraph()\" :width=620 :height=230></tradeview>
                </div>
                <div class=\"maingraph jostHead4\" style=\"width: 470px; /*max-width: 600px;*/ width: auto; display: inline-block; text-align: left\">
                    <div class=\"padding16\">
                        Thanks for being an Alpha v1.1 tester.<br />We appreciate you. <br />
                        <b>Please note:</b><br /><br />
                         Blue chart graphs should appear quickly, they represent the LP token price, as you are used to seeing on Yield Monitor already.<br />
                        <br />
                        User token and Price charts will take some time to appear (up to 15 minutes) during your first connect.<br />
                        This is because Yield Monitor is fetching all chain data in this initial connect.<br />
                    </div>
                </div>
            </div>
            <div v-for=\"lpCoin in sortedCoinData\" v-if=\"\" style=\"\" class=\"maingraph\">
            {% endverbatim %}
               {% include 'component/chart.twig'  with {'wallet': true}  %}
            {% verbatim %}
            </div>
        </div>

        <br />
    </div>
</div>

</div>

<div class=\"hide-on-large-only mobile-filler\"></div>
</div>
</div><script>

    //const rawData = '';

    //let decodedData = JSON.parse(rawData);

    var app = new Vue({
        el: '#mydiv',
        data: {
            allCoinData: [],
            hideCoinValues: false,
            HighestFirst: false,
            noData: false,
            limitNumber: 20,
            currentPage: 0,
            searchTerm: \"\",
            favorites: [],
            walletIds: [],
            farmData: {},
            networkFilter: \"all\",
            totalUsd: {},
            walletId: '{% endverbatim %}{{walletAddress}}{% verbatim %}',
            walletinput: '',
            highestTimestamp : 0,
            lastTotalForLp: {},
            lastPercentageForLp: {},
            done:0,
            searchClass: 'hide',
        },
        watch: {
            lastTotalForLp: function (val) {

            },
        },
        components: {
            highcharts: HighchartsVue.Chart,
           /*tradeview: tradeview*/
        },
        computed: {
            sortedCoinData() {
                var sortable = [...this.allCoinData];
                this.totalUsd = {};

                 if (this.networkFilter !== 'all') {
                    var tmpSortable = sortable
                        .filter((element) => (element.network === this.networkFilter));
                    sortable = tmpSortable;
                }

                if (!this.HighestFirst)
                    return sortable
                        .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                        .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);
//return sortable;
                sortable.sort(function (a, b) {
                    if (parseFloat(a.result) >= parseFloat(b.result)) return -1;
                    if (parseFloat(a.result) <= parseFloat(b.result)) return 1;
                    return 0;
                });

                // console.log(news);
                return sortable
                    .filter((element) => (element.symbol0Name.includes(this.searchTerm) || element.symbol1Name.includes(this.searchTerm)))
                    .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

            }
        },
        methods: {
            searchChanged(event) {
                if (event.target.value == 'lpresult') {
                    this.HighestFirst=true;
                    return
                }
                this.HighestFirst=false;
                return;
            },
            changeFilterNetwork(event) {
                this.networkFilter = event.target.value;
                this.currentPage = 0;
            },
             getCurrentDisplayPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 1000000) {
                    return (temp / 1000).toFixed(0) + 'K';
                }
                if (temp > 10000) {
                    return (temp / 1000).toFixed(2) + 'K';
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },
            getCurrentPrice (coin) {
                return this.lastTotalForLp[coin.id];
            },
            getCurrenPctResult (coin){
                return this.lastPercentageForLp[coin.id];
            },
            setWallet(){
                this.walletId = this.walletinput;
                this.loadWallet();
            },
            notExpiredToOld(coin) {
                var diff = this.highestTimestamp - coin[0].time;

                if (diff == 0) {
                    return true;
                }
                if (diff < 21000) {
                    return true;
                }
                return false;
            },
            loadWallet() {
                let walletAddress = this.walletId;
                axios.get('/wallet/lp/bsc/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        if (response.data['lps'] !== undefined)
                        this.walletIds = [...this.walletIds, ...response.data['lps']];
                       
                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });

                axios.get('/wallet/lp/avaxc/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        if (response.data['lps'] !== undefined)
                        this.walletIds = [...this.walletIds, ...response.data['lps']];
                       

                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });

                axios.get('/wallet/lp/fantom/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        if (response.data['lps'] !== undefined)
                        this.walletIds = [...this.walletIds, ...response.data['lps']];
                        
                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });

                axios.get('/wallet/farm/history/'+walletAddress)
                    .then(response =>  {
                        this.done ++;
                        
                        for(item in response.data) {
                            this.farmData[item] = response.data[item];
                        }
                        
                        if (this.done === 4)
                        this.fetchFavoriteLpData(0);
                    });
            },
            getTotalGraph() {
                // find largest farm data
                    // run these values,
                        // loop each farm to add total.
                let largest = 0;
                let largestId = 0;
                let timeBasedArray = [];
                let resultArray = [];
                let countResults = {};

                for (farmId in this.farmData) {
                    if (this.farmData[farmId].length > largest) {
                        largest = this.farmData[farmId].length;
                        largestId = farmId;
                    }
                    timeBasedArray[farmId] = [];

                    for(dataKey in this.farmData[farmId]) {
                        timeBasedArray[farmId][this.farmData[farmId][dataKey].time] = this.farmData[farmId][dataKey];
                    }


                }

                for (arrayKey in this.farmData[largestId]) {
                    const currentTimestamp = this.farmData[largestId][arrayKey].time;
                    resultArray[currentTimestamp] = 0;

                    if (currentTimestamp > this.highestTimestamp) {
                        this.highestTimestamp = currentTimestamp;
                    }


                    for (farmId in this.farmData) {
                        if (timeBasedArray[farmId][currentTimestamp]) {
                            resultArray[currentTimestamp] += parseFloat(timeBasedArray[farmId][currentTimestamp].value) *  parseFloat(timeBasedArray[farmId][currentTimestamp].price);

                            if (countResults[currentTimestamp] === undefined) {
                                countResults[currentTimestamp] = 0;
                            }
                            if (parseFloat(timeBasedArray[farmId][currentTimestamp].value) > 0)
                                countResults[currentTimestamp] ++;

                        }
                    }

                }
//console.log(resultArray);

                let finalArray = [];
                let prevTime = 0;
                let prevItem = {};
                let item1Back = null;
                let item2Back = null;
                for(time in resultArray) {

                    // current
                    if (item2Back !== null) {
                       // console.log(countResults[item2Back.time], countResults[item1Back.time], countResults[time]);
                        if (countResults[item1Back.time] < countResults[time] && countResults[item1Back.time] < countResults[item2Back.time] ){
                        //    console.log('neinn', item1Back.time);
                        } else {
                            finalArray.push(item1Back); // push prev item
                        }
                    }


                    item2Back = item1Back;
                    item1Back = {'time': parseInt(time), 'value': resultArray[time]};

                   /* if (prevTime !== 0 && countResults[prevTime] > countResults[time]) {
                        prevItem = {};
                        continue;
                    }
                    if (prevItem !== {}) {
                        finalArray.push(prevItem);
                    }


                    prevItem = {'time': parseInt(time), 'value': resultArray[time]};*/
                }

                finalArray.push(item1Back);
              //  return [{'time': 0, 'value': 0}];
                return finalArray;
            },
            getCoinTotal(symbol) {
                var tot = 0.0;
                const price = symbol.price[symbol.price.length -1].value;

                for (farmk in symbol.farms) {
                    const farm = symbol.farms[farmk];
                    if (this.farmData[farm.id] !== undefined) {

                        tot += parseFloat(this.farmData[farm.id][0].value);
                        this.totalUsd[farm.id] = price * tot;
                    }
                }

                return (tot * price).toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                });
            },
            getTotalUsd() {
                let totalCounted = 0;

                for(lpId in this.allCoinData) {
                    const lp = this.allCoinData[lpId];
                    const price = parseFloat(lp.price[lp.price.length -1].value);

                    for (farmk in lp.farms) {
                        const farm = lp.farms[farmk];
                        if (this.farmData[farm.id] !== undefined) {
                            totalCounted += price * parseFloat(this.farmData[farm.id][0]['value']);
                        }
                    }
                }


                return totalCounted.toLocaleString('en-US', {
                    minimumIntegerDigits: 2,
                    useGrouping: false
                });
            },
            favoritesLoaded(data) {
           // isConnected = '0x78d4498970F099e3C94cA8e6Bd90C5795D076041';


                for (var favoritekey in data) {
                    this.favorites.push(data[favoritekey].coinLP.id);
                }
               // this.fetchFavoriteLpData();
            },
            doSearch(event) {
                if (event.target.value.length === 0) {
                    this.searchTerm = '';
                }

                if (event.target.value.length >= 3) {
                    this.searchTerm = event.target.value.toUpperCase();
                }
            },
            changeAmountPerPage(event) {
                this.currentPage = 0;
                this.limitNumber = event.target.value;
            },
            addToFavorite(id) {
                if (!isConnected) {
                    alert('Please connect your wallet first');
                   // alert('Please connect your wallet first');
                } else {
                    axios.get('/lp/favorite/'+id+'/'+isConnected)
                        .then(response =>  {
                            this.favorites.push(id);
                        });
                }
            },
            getdatatest(coin, title = 'Price per share', ytext = 'USD') {
                return coin;
            },
            getLpData(coin) {
                let resultArray = []
                for(farmKey in coin.farms) {
                   const farm = coin.farms[farmKey];
                    if (this.farmData[farm.id]) {
                        for(arrayKey in this.farmData[farm.id]) {
                            const cur = this.farmData[farm.id][arrayKey];
                            if (cur.value === '-1') {
                                continue;
                            }
                            if (resultArray[cur.time] === undefined) resultArray[cur.time] = 0;

                            resultArray[cur.time] += parseFloat(cur.value);
                        }
                    }
                }

                let finalArray = [];
                for(time in resultArray) {
                    finalArray.push({'time': parseInt(time), 'value': resultArray[time]});
                }

                return finalArray;
            },
            getLpPriceData(coin) {
                let resultArray = []
                for(farmKey in coin.farms) {
                   const farm = coin.farms[farmKey];
                    if (this.farmData[farm.id]) {
                        for(arrayKey in this.farmData[farm.id]) {
                            const cur = this.farmData[farm.id][arrayKey];
                            if (cur.value === '-1') {
                                continue;
                            }
                            if (resultArray[cur.time] === undefined) resultArray[cur.time] = 0;
                            resultArray[cur.time] += (parseFloat(cur.value) * parseFloat(cur.price));
                        }
                    }
                }

                let finalArray = [];
                for(time in resultArray) {
                    finalArray.push({'time': parseInt(time), 'value': resultArray[time]});
                }

                let shouldRefresh = false;
                if (this.lastTotalForLp[coin.id] === undefined) {
                    shouldRefresh = true;
                }
                if (finalArray.length > 2) {
                    this.lastTotalForLp[coin.id] = finalArray[finalArray.length - 1].value.toFixed(2);
                    var now =finalArray[finalArray.length - 1].value
                    var last =finalArray[0].value;
                    if (now > last) {
                        var diff = now - last;
                        var gain = diff/now*100;
                    } else {
                        var diff = last - now;
                        var gain = -(diff/last*100);
                    }

                    this.lastPercentageForLp[coin.id] = gain.toFixed(2);
                } else {
                    this.lastPercentageForLp[coin.id] = 'tbd';
                    shouldRefresh = false;
                }
                if (shouldRefresh) {
                    this.\$forceUpdate();
                }

                return finalArray;
            },
            fetchFavoriteLpData(page) {
                axios.post(\"/getpricesforlps\",{lps: this.walletIds, 'includeFarms': true, 'includeInactive': true})
                    .then(response =>  {
                        let amountReturned = 1;
                        this.allCoinData = [];
                        for (var lpName in response.data) {
                            amountReturned++;
                            this.allCoinData.push(response.data[lpName]);
                        }

                        if (amountReturned > 10) {
                          //  this.fetchNextData(page+1);
                        }

                        if (this.allCoinData.length === 0) {
                        this.noData = true;
                        return;
                        }

                        this.searchClass = 'show';
                    });
            }
        },
        mounted() {
           /* this.fetchNextData(0);*/

            if (this.walletId !== '') {
                this.loadWallet();
            }

        }
    });

</script>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, {});
    });
</script>
{% endverbatim %}

{% include 'layout/footer.twig' %}", "pages/wallet.twig", "/var/www/default/templates/pages/wallet.twig");
    }
}
