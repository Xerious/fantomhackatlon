<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* pages/detail.twig */
class __TwigTemplate_ee363acc455b883fc05060212725a632e86758a3162be68d7b81db8ae1abb2aa extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "pages/detail.twig"));

        // line 1
        $this->loadTemplate("layout/header.twig", "pages/detail.twig", 1)->display($context);
        // line 176
        echo "
<div id=\"mydiv\">

    <div id='inner-flex-container'>
<div class=\"contentHeader\">

</div>
<div class=\"pageContent detailPage\">
    <div>

        <div class=\"graphHolder\">
            <div v-if=\"sortedCoinData.length == 0\"> Loading ...</div>

            <div  class=\"maingraph bigGraph\" v-if=\"currentCoin\">
                <div class=\"cardTop\">
                    <div class=\"networkInfo graphPadding\">
                        <div style=\"float: left;\">
                            <div class=\"\">
                                <div v-if=\"currentCoin.network == 'bsc'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/bsc.png\" class=\"networkBodyLogo\" /> Bsc</div>
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'fantom'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/fantom.png\" class=\"networkBodyLogo\" />Ftm</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'avaxc'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/avalanche.png\" class=\"networkBodyLogo\" />Avax</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'poly'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/polygon.png\" class=\"networkBodyLogo\" />Poly</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'mainnet'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/ethernuem.png\" class=\"networkBodyLogo\" />Eth</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                            </div>
                        </div>
                        <div style=\"float: right; margin-top: 18px;\">
                            <span class=\"chartValue jostHead5 positive\" v-if=\"getCurrenPctResult(currentCoin) >= 0 \">
                               \$ {{getCurrentPrice(currentCoin)}}
                            </span>
                            <span class=\"chartValue jostHead5\" v-if=\"getCurrenPctResult(currentCoin) < 0 \">
                               \$ {{getCurrentPrice(currentCoin)}}
                            </span>
                            <span v-if=\"getCurrenPctResult(currentCoin) >= 0 \" class=\"positive resultFloat chartResult jostSub2\">
                               <span class=\"material-icons\">trending_up</span>
                                {{getCurrenPctResult(currentCoin)}}%
                            </span>
                            <span v-if=\"getCurrenPctResult(currentCoin) < 0 \"  class=\"negative resultFloat chartResult jostSub2\">
                            <span class=\"material-icons\">trending_down</span>
                                {{getCurrenPctResult(currentCoin)}}%
                            </span>
                        </div>

                        <div style=\"clear: both\"></div>
                    </div>
                    <div class=\"symbolsText jostHead6\">
                        {{currentCoin.symbol0Name}} - {{currentCoin.symbol1Name}}
                    </div>
                </div>
                <div class=\"graphHolderInner graphPadding\">
                    <tradeview :containerid=\"'chart'+currentCoin.id\" :pricedata=\"currentCoin.price\" :width=\"610\" :height=\"400\"></tradeview>
                </div>
                <div>
                <div style=\"width: 49%; display: inline-block\">
                    <span class=\"jostHead1\">{{currentCoin.symbol0Name}}</span>
                    <tradeview :containerid=\"'chartcoin1'+currentCoin.id\" :pricedata=\"currentCoin.coins0\" :width=\"290\" :height=\"200\"></tradeview>
                </div>
                <div style=\"width: 49%; display: inline-block\">
                    <span class=\"jostHead1\">{{currentCoin.symbol1Name}}</span>
                    <tradeview :containerid=\"'chartcoin1'+currentCoin.id\" :pricedata=\"currentCoin.coins1\" :width=\"290\" :height=\"200\"></tradeview>
                </div>
                </div>
            </div>


                <div  class=\" maingraphTrans\">

                    <div  class=\"maingraph graphPadding\" style=\"height: 575px;\">
                        <div>
                            <span class=\"jostHead3\">Impermanent Loss</span>
                            <div style=\"\">
                                <select name=\"ilperiod\" @change=\"ilPeriodChanged(\$event)\">
                                    <option value=\"4\">4H</option>
                                    <option value=\"8\">8H</option>
                                    <option value=\"24\">1day</option>
                                    <option value=\"48\">2 days</option>
                                    <option value=\"120\">5 days</option>
                                    <option value=\"240\">10 days</option>
                                </select>
                            </div>


                        </div>
                            <br />
                            <div style=\"display: inline-block; width: 49%; text-align: left\">
                                <span class=\"jostHead4\">Start</span>
                                <hr >
                                <div class=\"tokenName jostHead6\">{{coin0Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{oldTotalCoin1}} ({{oldTotalCoin1Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\"> {{coin1Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{oldTotalCoin0}} ({{oldTotalCoin0Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\">Total (BUSD)</div>
                                <div class=\"tokenResult jostSub2\" style=\"font-size: 12px;\">{{oldTotalPrice}} </div>
                                <hr >
                            </div>
                            <div style=\"display: inline-block; width: 49%; text-align: left\">
                                <span  class=\"jostHead4\">Current</span>
                                <hr >
                                <div class=\"tokenName jostHead6\">{{coin0Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{currentTotalCoin1}} ({{currentTotalCoin1Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\">{{coin1Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{currentTotalCoin0}} ({{currentTotalCoin0Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\">Total (BUSD)</div>
                                <div class=\"tokenResult jostSub2\" style=\"font-size: 12px;\">{{currentTotalPrice}} BUSD</div>
                                <hr >
                            </div>
                        <div style=\"text-align: left\">
                            <span  class=\"jostHead3\">Result</span>
                            <hr >
                        </div>
                            <div>
                                <div style=\"display: inline-block; width: 49%; vertical-align: top; text-align: left\">
                                    <div class=\"tokenName jostHead6\">{{coin0Name}}</div>
                                    <div class=\"jostSub2\">
                                        <yieldtext :text=\"calcDiff(currentTotalCoin1, oldTotalCoin1).toFixed(3) \" :number=\"calcDiff(currentTotalCoin1, oldTotalCoin1).toFixed(3)\"></yieldtext> (
                                        <yieldtext :text=\"calcDiff(currentTotalCoin1Price, oldTotalCoin1Price).toFixed(3) + ' \$'\" :number=\"calcDiff(currentTotalCoin1Price, oldTotalCoin1Price).toFixed(3)\"></yieldtext> )
                                    </div>
                                </div>
                                <div style=\"display: inline-block; width: 49%; vertical-align: top\">
                                    <div class=\"tokenName jostHead6\">{{coin1Name}}</div>
                                    <div class=\"jostSub2\">
                                    <yieldtext :text=\"calcDiff(currentTotalCoin0, oldTotalCoin0).toFixed(2) \" :number=\"calcDiff(currentTotalCoin0, oldTotalCoin0).toFixed(2)\"></yieldtext> (
                                    <yieldtext :text=\"calcDiff(currentTotalCoin0Price, oldTotalCoin0Price).toFixed(2) + ' \$'\" :number=\"calcDiff(currentTotalCoin0Price, oldTotalCoin0Price).toFixed(2)\"></yieldtext> )
                                    </div>
                                </div>

                            </div>
                        <div class=\"totalText jostHead3\" style=\"display: inline-block; width: 99%; vertical-align: top; text-align: left\">
                             <span  class=\"jostHead3\">Total:</span>
                            <yieldtext :text=\"calcDiff(currentTotalPrice, oldTotalPrice).toFixed(3) + ' \$'\" :number=\"calcDiff(currentTotalPrice, oldTotalPrice).toFixed(3)\"></yieldtext>
                        </div>
                    </div>
                    <div  class=\"maingraph\">
                            <div class=\"symbolsText jostHead6 text-left\">
                                Farms
                            </div>
                            <div class=\"chart-farm-holder\">
                            <div v-for=\"farm in farms\" class=\"row text-left chart-farm-row jostHead4 pad8\">
                                <div class=\"farm-chart-name\">{{farm.name}}</div> <div class=\"farm-chart-value text-right\"><span v-if=\"farmData[farm.id] && notExpiredToOld(farmData[farm.id])\">{{farmData[farm.id][0].value}}</span></div>
                            </div>
                        </div>
                        <div class=\"pad8\"></div>
                    </div>
                </div>


        </div>

        <br />
    </div>
</div>
</div>
</div>

<script>

    const rawData = '";
        echo (isset($context["replaceWithData"]) || array_key_exists("replaceWithData", $context) ? $context["replaceWithData"] : (function () { throw new RuntimeError('Variable "replaceWithData" does not exist.', 176, $this->source); })());
        // line 177
        echo "';
    const replaceWithFarmData = '";
        echo (isset($context["replaceWithFarmData"]) || array_key_exists("replaceWithFarmData", $context) ? $context["replaceWithFarmData"] : (function () { throw new RuntimeError('Variable "replaceWithFarmData" does not exist.', 177, $this->source); })());
        // line 549
        echo "';

    let decodedData = JSON.parse(rawData);
    let decodedFarmData = JSON.parse(replaceWithFarmData);

    var app = new Vue({
        el: '#mydiv',
        data: {
            allCoinData: [],
            hideCoinValues: false,
            HighestFirst: false,
            limitNumber: 20,
            currentPage: 0,
            searchTerm: \"\",
            favorites: [],
            oldTotalPrice: 0.0,
            oldTotalCoin1: 0.0,
            oldTotalCoin0: 0.0,
            oldTotalCoin1Price: 0.0,
            oldTotalCoin0Price: 0.0,
            currentTotalPrice: 0.0,
            currentTotalCoin1: 0.0,
            currentTotalCoin0: 0.0,
            currentTotalCoin1Price: 0.0,
            currentTotalCoin0Price: 0.0,
            coin0Name: 'coin0',
            coin1Name: 'coin1',
            ilPeriod: 24,
            farms: decodedFarmData,
            currentCoin: {},
            farmValues: {},
            coinsInWallet: 0,
            farmData: []
        },
        components: {
            highcharts: HighchartsVue.Chart
        },
        computed: {
            sortedCoinData() {
                var args = this.allCoinData;
                var sortable = this.allCoinData;

                if (!this.HighestFirst)
                    return sortable
                        .filter((element) => element[0].includes(this.searchTerm))
                        .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

                sortable.sort(function (a, b) {
                    if (parseFloat(a[1].result) >= parseFloat(b[1].result)) return -1;
                    if (parseFloat(a[1].result) <= parseFloat(b[1].result)) return 1;
                    return 0;
                });

                // console.log(news);
                return sortable
                    .filter((element) => element[0].includes(this.searchTerm))
                    .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

            }
        },
        methods: {
            getCurrentPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 10000) {
                    return temp.toFixed(0);
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },
            getCurrenPctResult(coin) {
                if (coin.price === undefined) return 'tbd';
                const now = coin.price[coin.price.length - 1].value;
                const last = coin.price[0].value;
                if (now > last) {
                    var diff = now - last;
                    var gain = diff/now*100;
                } else {
                    var diff = last - now;
                    var gain = -(diff/last*100);
                }
                return gain.toFixed(2);
            },
            addToFavorite(id) {
                if (!isConnected) {
                    alert('Please connect your wallet first');
                    // alert('Please connect your wallet first');
                } else {
                    axios.get('/lp/favorite/' + id + '/' + isConnected)
                        .then(response => {
                            this.favorites.push(id);
                        });
                }
            },
            favoritesLoaded(data) {
                this.startLpFetch();
                for (var favoritekey in data) {
                    this.favorites.push(data[favoritekey].coinLP.id);
                }
            },
            startLpFetch() {
                return ;
                if (YMCurrentAddress === '') {
                    return ;
                }
                isConnected = '0x78d4498970F099e3C94cA8e6Bd90C5795D076041';
                YMCurrentAddress = '0x78d4498970F099e3C94cA8e6Bd90C5795D076041';
                for (farm in this.farms) {
                    // const currentFarm = this.farmAddresses[this.farms[farm].name];
                    const that = this;
                    const farmName = this.farms[farm].name;
                    this.doLpCall(this.farms[farm].name, this.farms[farm].poolNumber, this.farms[farm]).then((res) => {
                        this.farmValues[farmName] = res;
                        this.\$forceUpdate();
                    });

                }

                this.coinsInWallet = this.loadLpInWallet();

            },
            loadLpInWallet: async(network, lpAddress) => {
                if (network === 'Pangolin') {
                    const provider = new ethers.providers.Web3Provider(window.ethereum)
                    const signer = provider.getSigner()
                    const lpAbi = [{\"type\":\"constructor\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"inputs\":[]},{\"type\":\"event\",\"name\":\"Approval\",\"inputs\":[{\"type\":\"address\",\"name\":\"owner\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"spender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Burn\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount0\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Mint\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount0\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Swap\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount0In\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1In\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount0Out\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1Out\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Sync\",\"inputs\":[{\"type\":\"uint112\",\"name\":\"reserve0\",\"internalType\":\"uint112\",\"indexed\":false},{\"type\":\"uint112\",\"name\":\"reserve1\",\"internalType\":\"uint112\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Transfer\",\"inputs\":[{\"type\":\"address\",\"name\":\"from\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"bytes32\",\"name\":\"\",\"internalType\":\"bytes32\"}],\"name\":\"DOMAIN_SEPARATOR\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"MINIMUM_LIQUIDITY\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"bytes32\",\"name\":\"\",\"internalType\":\"bytes32\"}],\"name\":\"PERMIT_TYPEHASH\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"allowance\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"bool\",\"name\":\"\",\"internalType\":\"bool\"}],\"name\":\"approve\",\"inputs\":[{\"type\":\"address\",\"name\":\"spender\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"balanceOf\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"amount0\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"amount1\",\"internalType\":\"uint256\"}],\"name\":\"burn\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint8\",\"name\":\"\",\"internalType\":\"uint8\"}],\"name\":\"decimals\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"factory\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint112\",\"name\":\"_reserve0\",\"internalType\":\"uint112\"},{\"type\":\"uint112\",\"name\":\"_reserve1\",\"internalType\":\"uint112\"},{\"type\":\"uint32\",\"name\":\"_blockTimestampLast\",\"internalType\":\"uint32\"}],\"name\":\"getReserves\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"initialize\",\"inputs\":[{\"type\":\"address\",\"name\":\"_token0\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"_token1\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"kLast\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"liquidity\",\"internalType\":\"uint256\"}],\"name\":\"mint\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"string\",\"name\":\"\",\"internalType\":\"string\"}],\"name\":\"name\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"nonces\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"permit\",\"inputs\":[{\"type\":\"address\",\"name\":\"owner\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"spender\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"deadline\",\"internalType\":\"uint256\"},{\"type\":\"uint8\",\"name\":\"v\",\"internalType\":\"uint8\"},{\"type\":\"bytes32\",\"name\":\"r\",\"internalType\":\"bytes32\"},{\"type\":\"bytes32\",\"name\":\"s\",\"internalType\":\"bytes32\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"price0CumulativeLast\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"price1CumulativeLast\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"skim\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"swap\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount0Out\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"amount1Out\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"},{\"type\":\"bytes\",\"name\":\"data\",\"internalType\":\"bytes\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"string\",\"name\":\"\",\"internalType\":\"string\"}],\"name\":\"symbol\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"sync\",\"inputs\":[],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"token0\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"token1\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"totalSupply\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"bool\",\"name\":\"\",\"internalType\":\"bool\"}],\"name\":\"transfer\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"bool\",\"name\":\"\",\"internalType\":\"bool\"}],\"name\":\"transferFrom\",\"inputs\":[{\"type\":\"address\",\"name\":\"from\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"}],\"constant\":false}];
                    const lpContract = new ethers.Contract(lpAddress, farmInfo.abi, signer);
                    var userInfo = await lpContract.balanceOf (YMCurrentAddress);

                    var decimals = 18;
                    return userInfo / 10 ** decimals;
                }
            },
            doLpCall: async(network, poolNumber, farm) => {
                return;
                const provider = new ethers.providers.Web3Provider(window.ethereum)
                const signer = provider.getSigner()
                if (network === 'auto' || network === 'pAuto') {
                    const abi = [{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"EmergencyWithdraw\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Withdraw\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"AUTO\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"AUTOMaxSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"AUTOPerBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"AUTOv2\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"contract IERC20\",\"name\":\"_want\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"_strat\",\"type\":\"address\"}],\"name\":\"add\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"burnAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"emergencyWithdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_to\",\"type\":\"uint256\"}],\"name\":\"getMultiplier\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"inCaseTokensGetStuck\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"massUpdatePools\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_inputAmt\",\"type\":\"uint256\"}],\"name\":\"migrateToAUTOv2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"ownerAUTOReward\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"pendingAUTO\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"contract IERC20\",\"name\":\"want\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"lastRewardBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"accAUTOPerShare\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"strat\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"poolLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"set\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"stakedWantTokens\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"startBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalAllocPoint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"updatePool\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userInfo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"shares\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"rewardDebt\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"withdrawAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}];
                    const chefContract = new ethers.Contract(farm.farmAddress, abi, signer);
                    var userInfo = await chefContract.userInfo(poolNumber, YMCurrentAddress);
                    const poolInfo = await chefContract.poolInfo(poolNumber);

                    const autoStratAbi = [{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_govAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_autoFarmAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_AUTOAddress\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_isCAKEStaking\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"_isAutoComp\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"_farmContractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_wantAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_token0Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_token1Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_earnedAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_uniRouterAddress\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"AUTOAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"autoFarmAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackRateMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackRateUL\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"controllerFee\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"controllerFeeMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"controllerFeeUL\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"convertDustToEarned\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_userAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"earn\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"earnedAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"earnedToAUTOPath\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"earnedToToken0Path\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"earnedToToken1Path\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"entranceFeeFactor\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"entranceFeeFactorLL\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"entranceFeeFactorMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"farm\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"farmContractAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"govAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"}],\"name\":\"inCaseTokensGetStuck\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isAutoComp\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isCAKEStaking\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"lastEarnBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"onlyGov\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pid\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_controllerFee\",\"type\":\"uint256\"}],\"name\":\"setControllerFee\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_entranceFeeFactor\",\"type\":\"uint256\"}],\"name\":\"setEntranceFeeFactor\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_govAddress\",\"type\":\"address\"}],\"name\":\"setGov\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_onlyGov\",\"type\":\"bool\"}],\"name\":\"setOnlyGov\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_buyBackRate\",\"type\":\"uint256\"}],\"name\":\"setbuyBackRate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"sharesTotal\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token0Address\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"token0ToEarnedPath\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token1Address\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"token1ToEarnedPath\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"uniRouterAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wantAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wantLockedTotal\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wbnbAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_userAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]

                    const strat = new ethers.Contract(poolInfo.strat, autoStratAbi, signer);
                    const totalShares = await  strat.sharesTotal();
                    const poolStaked = await  strat.wantLockedTotal() / 1e18;

                    return userInfo.shares / totalShares * poolStaked;
                }else
                if (network === 'TraderJoe') {
                    const abi = [{\"type\":\"constructor\",\"stateMutability\":\"nonpayable\",\"inputs\":[{\"type\":\"address\",\"name\":\"_lyd\",\"internalType\":\"contract LydToken\"},{\"type\":\"address\",\"name\":\"_electrum\",\"internalType\":\"contract ElectrumBar\"},{\"type\":\"address\",\"name\":\"_devaddr\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"_lydPerSec\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_startTimestamp\",\"internalType\":\"uint256\"}]},{\"type\":\"event\",\"name\":\"Deposit\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"pid\",\"internalType\":\"uint256\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"EmergencyWithdraw\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"pid\",\"internalType\":\"uint256\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"OwnershipTransferred\",\"inputs\":[{\"type\":\"address\",\"name\":\"previousOwner\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"SetDevAddress\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"newAddress\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"UpdateEmissionRate\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"_lydPerSec\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Withdraw\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"pid\",\"internalType\":\"uint256\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"BONUS_MULTIPLIER\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"add\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_allocPoint\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"_lpToken\",\"internalType\":\"contract IERC20\"},{\"type\":\"bool\",\"name\":\"_withUpdate\",\"internalType\":\"bool\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"deposit\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"dev\",\"inputs\":[{\"type\":\"address\",\"name\":\"_devaddr\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"devaddr\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract ElectrumBar\"}],\"name\":\"electrum\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"emergencyWithdraw\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"enterStaking\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"getMultiplier\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_from\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_to\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"leaveStaking\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract LydToken\"}],\"name\":\"lyd\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"lydPerSec\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"massUpdatePools\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"owner\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"pendingLyd\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"_user\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"lpToken\",\"internalType\":\"contract IERC20\"},{\"type\":\"uint256\",\"name\":\"allocPoint\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"lastRewardTimestamp\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"accLydPerShare\",\"internalType\":\"uint256\"}],\"name\":\"poolInfo\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"poolLength\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"renounceOwnership\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"set\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_allocPoint\",\"internalType\":\"uint256\"},{\"type\":\"bool\",\"name\":\"_withUpdate\",\"internalType\":\"bool\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"startTimestamp\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"totalAllocPoint\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"transferOwnership\",\"inputs\":[{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"updateEmissionRate\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_lydPerSec\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"updateMultiplier\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"multiplierNumber\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"updatePool\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"rewardDebt\",\"internalType\":\"uint256\"}],\"name\":\"userInfo\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"withdraw\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]}];
                    const chefContract = new ethers.Contract(farmInfo.address, abi, signer);
                    var userInfo = await chefContract.userInfo(poolNumber, YMCurrentAddress);
                    var decimals = 18;
                    return userInfo.amount / 10 ** decimals;
                }else
                if (network === 'Pangolin') {
                    // in farm
                    const pangolinLpABI = [{\"type\":\"constructor\",\"stateMutability\":\"nonpayable\",\"inputs\":[{\"type\":\"address\",\"name\":\"_rewardsToken\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"_stakingToken\",\"internalType\":\"address\"}]},{\"type\":\"event\",\"name\":\"OwnershipTransferred\",\"inputs\":[{\"type\":\"address\",\"name\":\"previousOwner\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Recovered\",\"inputs\":[{\"type\":\"address\",\"name\":\"token\",\"internalType\":\"address\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"RewardAdded\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"reward\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"RewardPaid\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"reward\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"RewardsDurationUpdated\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"newDuration\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Staked\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Withdrawn\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"balanceOf\",\"inputs\":[{\"type\":\"address\",\"name\":\"account\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"earned\",\"inputs\":[{\"type\":\"address\",\"name\":\"account\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"exit\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"getReward\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"getRewardForDuration\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"lastTimeRewardApplicable\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"lastUpdateTime\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"notifyRewardAmount\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"reward\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"owner\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"periodFinish\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"recoverERC20\",\"inputs\":[{\"type\":\"address\",\"name\":\"tokenAddress\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"tokenAmount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"renounceOwnership\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardPerToken\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardPerTokenStored\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardRate\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewards\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardsDuration\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract IERC20\"}],\"name\":\"rewardsToken\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"setRewardsDuration\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_rewardsDuration\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"stake\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"stakeWithPermit\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"deadline\",\"internalType\":\"uint256\"},{\"type\":\"uint8\",\"name\":\"v\",\"internalType\":\"uint8\"},{\"type\":\"bytes32\",\"name\":\"r\",\"internalType\":\"bytes32\"},{\"type\":\"bytes32\",\"name\":\"s\",\"internalType\":\"bytes32\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract IERC20\"}],\"name\":\"stakingToken\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"totalSupply\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"transferOwnership\",\"inputs\":[{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"userRewardPerTokenPaid\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"withdraw\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"}]}];
                    const lpContract = new ethers.Contract(farm.coinLP.contractAddr, pangolinLpABI, signer);
                    var userInfo = await lpContract.balanceOf (YMCurrentAddress);

                    var decimals = 18;
                    return userInfo / 10 ** decimals;
                } else {
                    const abi = [{\"inputs\":[{\"internalType\":\"contract CakeToken\",\"name\":\"_cake\",\"type\":\"address\"},{\"internalType\":\"contract SyrupBar\",\"name\":\"_syrup\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_devaddr\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_cakePerBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_startBlock\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"EmergencyWithdraw\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Withdraw\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"BONUS_MULTIPLIER\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"contract IBEP20\",\"name\":\"_lpToken\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"add\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"cake\",\"outputs\":[{\"internalType\":\"contract CakeToken\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"cakePerBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_devaddr\",\"type\":\"address\"}],\"name\":\"dev\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"devaddr\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"emergencyWithdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"enterStaking\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_to\",\"type\":\"uint256\"}],\"name\":\"getMultiplier\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"leaveStaking\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"massUpdatePools\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"migrate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"migrator\",\"outputs\":[{\"internalType\":\"contract IMigratorChef\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"pendingCake\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"contract IBEP20\",\"name\":\"lpToken\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"lastRewardBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"accCakePerShare\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"poolLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"set\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contract IMigratorChef\",\"name\":\"_migrator\",\"type\":\"address\"}],\"name\":\"setMigrator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"startBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"syrup\",\"outputs\":[{\"internalType\":\"contract SyrupBar\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalAllocPoint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"multiplierNumber\",\"type\":\"uint256\"}],\"name\":\"updateMultiplier\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"updatePool\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userInfo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"rewardDebt\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}];
                    const chefContract = new ethers.Contract(farm.farmAddress, abi, signer);
                    var userInfo = await chefContract.userInfo(poolNumber, YMCurrentAddress);
                    var decimals = 18;
                    return userInfo.amount / 10 ** decimals;
                }


                return 0;
                // const STAKING_POOL = new ethereum.Contract(farmInfo['address'], farmInfo['abi']);
                // const stakeTokenAddress = STAKING_POOL.userInfo(poolIndex, YMCurrentAddress);
                //  console.log(stakeTokenAddress)
                // var esta= .Contract(farmInfo['abi'], farmInfo['address']);
                //  console.log(ethereum);

                // const web3 = new Web3(\"hhttps://bsc-dataseed.binance.org\")

                //  var test = new web3.eth.Contract(farmInfo['abi'], farmInfo['address']);
                // console.log(test);
            },
            ilPeriodChanged(event) {
                console.log(event.target.value);
                this.ilPeriod = event.target.value;
                this.calculateIL();
            },
            loadLpBalances() {

            },
            calcDiff(var1, var2) {
                return var1 - var2;
            },
            calcDiffHold(var1, var2) {
                return (this.oldTotalCoin0 * 2);
            },
            calculateIL() {
                const hoursAgo = this.ilPeriod;

                const coin = this.allCoinData[0][1];

                this.coin0Name = coin.symbol0Name;
                this.coin1Name = coin.symbol1Name;

                const latestTS = coin.price[coin.price.length - 1]['time'];

                const yest = latestTS - (hoursAgo * 3600);
                //find con from yest

                this.oldTotalPrice = this.findDateTimeObjectPrice(coin.price, yest).toFixed(2);
                this.oldTotalCoin1 = this.findDateTimeObject(coin.coins1, yest).toFixed(7);
                this.oldTotalCoin0 = this.findDateTimeObject(coin.coins0, yest).toFixed(7);
                this.oldTotalCoin1Price = this.findDateTimeObject(coin.coins0price, yest).toFixed(2);
                this.oldTotalCoin0Price = this.findDateTimeObject(coin.coins1price, yest).toFixed(2);
                this.currentTotalPrice = coin.price[coin.price.length - 1]['value'].toFixed(3);
                this.currentTotalCoin1 = coin.coins1[coin.coins1.length - 1]['value'].toFixed(7);
                this.currentTotalCoin0 = coin.coins0[coin.coins0.length - 1]['value'].toFixed(7);
                this.currentTotalCoin1Price = coin.coins0price[coin.coins0price.length - 1]['value'].toFixed(2);
                this.currentTotalCoin0Price = coin.coins1price[coin.coins1price.length - 1]['value'].toFixed(2);
                console.log(this.oldTotalPrice);
                console.log(this.currentTotalPrice);
            },
            findDateTimeObject(array, tofind) {
                for (i = array.length - 1; i > 0; i--) {
                    const now = array[i];

                    if (now['time'] < tofind) {
                        return array[i]['value'];
                    }
                }

                return 0;
            },
            findDateTimeObjectPrice(array, tofind) {
                for (i = array.length - 1; i > 0; i--) {
                    const now = array[i];

                    if (now['time'] < tofind) {
                        return now['value'];
                    }
                }
                return 0;
            },
            doSearch(event) {
                if (event.target.value.length === 0) {
                    this.searchTerm = '';
                }

                if (event.target.value.length >= 3) {
                    this.searchTerm = event.target.value.toUpperCase();
                }
            },
            changeAmountPerPage(event) {
                this.currentPage = 0;
                this.limitNumber = event.target.value;
            },
            getChartOptions(coin, title = 'Price per share', ytext = 'USD') {
                return {
                    chart: {
                        zoomType: 'x',
                        defaultSeriesType: 'area',
                        backgroundColor: 'rgba(0,0,0,0.0)'
                    },
                    title: {
                        text: title
                    },
                    /* subtitle: {
                         text: document.ontouchstart === undefined ?
                             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                     },*/
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: ytext
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        data: coin
                    }]
                }
            },
            fetchNextData(page) {
                /*  axios.get(\"/getprices/\" + page)
                      .then(response =>  {
                          let amountReturned = 1;
                          for (var lpName in response.data) {
                              amountReturned++;
                              this.allCoinData.push([lpName, response.data[lpName]]);
                          }

                          if (amountReturned > 10) {
                              this.fetchNextData(page+1);
                          }
                      });*/
            }
        },
        mounted() {
            // this.fetchNextData(0);
            for (var lpName in decodedData) {
                this.allCoinData.push([lpName, decodedData[lpName]]);
                this.currentCoin = decodedData[lpName];
                this.calculateIL();
            }
            // setTimeout(this.startLpFetch(),1000);
        }
    })



    async function loadMultipleAvaxSynthetixPools(App, tokens, prices, pools) {
        let totalStaked  = 0, totalUserStaked = 0, individualAPRs = [];
        const infos = await Promise.all(pools.map(p =>
            loadAvaxSynthetixPoolInfo(App, tokens, prices, p.abi, p.address, p.rewardTokenFunction, p.stakeTokenFunction)));
        for (const i of infos) {
            let p = await printSynthetixPool(App, i, \"avax\");
            totalStaked += p.staked_tvl || 0;
            totalUserStaked += p.userStaked || 0;
            if (p.userStaked > 0) {
                individualAPRs.push(p.userStaked * p.apr / 100);
            }
        }
        let totalApr = totalUserStaked == 0 ? 0 : individualAPRs.reduce((x,y)=>x+y, 0) / totalUserStaked;
        return { staked_tvl : totalStaked, totalUserStaked, totalApr };
    }
";
        echo "
</script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, {});
    });
</script>
";
        // line 557
        $this->loadTemplate("layout/footer.twig", "pages/detail.twig", 557)->display($context);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "pages/detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  606 => 557,  223 => 549,  219 => 177,  42 => 176,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% include 'layout/header.twig' %}
{% verbatim %}
<div id=\"mydiv\">

    <div id='inner-flex-container'>
<div class=\"contentHeader\">

</div>
<div class=\"pageContent detailPage\">
    <div>

        <div class=\"graphHolder\">
            <div v-if=\"sortedCoinData.length == 0\"> Loading ...</div>

            <div  class=\"maingraph bigGraph\" v-if=\"currentCoin\">
                <div class=\"cardTop\">
                    <div class=\"networkInfo graphPadding\">
                        <div style=\"float: left;\">
                            <div class=\"\">
                                <div v-if=\"currentCoin.network == 'bsc'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/bsc.png\" class=\"networkBodyLogo\" /> Bsc</div>
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'fantom'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/fantom.png\" class=\"networkBodyLogo\" />Ftm</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'avaxc'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/avalanche.png\" class=\"networkBodyLogo\" />Avax</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'poly'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/polygon.png\" class=\"networkBodyLogo\" />Poly</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                                <div v-if=\"currentCoin.network == 'mainnet'\">
                                    <div class=\"jostHead4\"><img src=\"/public/networks/ethernuem.png\" class=\"networkBodyLogo\" />Eth</div><br />
                                    <div class=\"jostSub1 margintop4\"><img :src=\"'/public/farms/'+currentCoin.origin+'.png'\" class=\"networkBodyLogo\" /> {{currentCoin.origin}}</div>
                                </div>
                            </div>
                        </div>
                        <div style=\"float: right; margin-top: 18px;\">
                            <span class=\"chartValue jostHead5 positive\" v-if=\"getCurrenPctResult(currentCoin) >= 0 \">
                               \$ {{getCurrentPrice(currentCoin)}}
                            </span>
                            <span class=\"chartValue jostHead5\" v-if=\"getCurrenPctResult(currentCoin) < 0 \">
                               \$ {{getCurrentPrice(currentCoin)}}
                            </span>
                            <span v-if=\"getCurrenPctResult(currentCoin) >= 0 \" class=\"positive resultFloat chartResult jostSub2\">
                               <span class=\"material-icons\">trending_up</span>
                                {{getCurrenPctResult(currentCoin)}}%
                            </span>
                            <span v-if=\"getCurrenPctResult(currentCoin) < 0 \"  class=\"negative resultFloat chartResult jostSub2\">
                            <span class=\"material-icons\">trending_down</span>
                                {{getCurrenPctResult(currentCoin)}}%
                            </span>
                        </div>

                        <div style=\"clear: both\"></div>
                    </div>
                    <div class=\"symbolsText jostHead6\">
                        {{currentCoin.symbol0Name}} - {{currentCoin.symbol1Name}}
                    </div>
                </div>
                <div class=\"graphHolderInner graphPadding\">
                    <tradeview :containerid=\"'chart'+currentCoin.id\" :pricedata=\"currentCoin.price\" :width=\"610\" :height=\"400\"></tradeview>
                </div>
                <div>
                <div style=\"width: 49%; display: inline-block\">
                    <span class=\"jostHead1\">{{currentCoin.symbol0Name}}</span>
                    <tradeview :containerid=\"'chartcoin1'+currentCoin.id\" :pricedata=\"currentCoin.coins0\" :width=\"290\" :height=\"200\"></tradeview>
                </div>
                <div style=\"width: 49%; display: inline-block\">
                    <span class=\"jostHead1\">{{currentCoin.symbol1Name}}</span>
                    <tradeview :containerid=\"'chartcoin1'+currentCoin.id\" :pricedata=\"currentCoin.coins1\" :width=\"290\" :height=\"200\"></tradeview>
                </div>
                </div>
            </div>


                <div  class=\" maingraphTrans\">

                    <div  class=\"maingraph graphPadding\" style=\"height: 575px;\">
                        <div>
                            <span class=\"jostHead3\">Impermanent Loss</span>
                            <div style=\"\">
                                <select name=\"ilperiod\" @change=\"ilPeriodChanged(\$event)\">
                                    <option value=\"4\">4H</option>
                                    <option value=\"8\">8H</option>
                                    <option value=\"24\">1day</option>
                                    <option value=\"48\">2 days</option>
                                    <option value=\"120\">5 days</option>
                                    <option value=\"240\">10 days</option>
                                </select>
                            </div>


                        </div>
                            <br />
                            <div style=\"display: inline-block; width: 49%; text-align: left\">
                                <span class=\"jostHead4\">Start</span>
                                <hr >
                                <div class=\"tokenName jostHead6\">{{coin0Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{oldTotalCoin1}} ({{oldTotalCoin1Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\"> {{coin1Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{oldTotalCoin0}} ({{oldTotalCoin0Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\">Total (BUSD)</div>
                                <div class=\"tokenResult jostSub2\" style=\"font-size: 12px;\">{{oldTotalPrice}} </div>
                                <hr >
                            </div>
                            <div style=\"display: inline-block; width: 49%; text-align: left\">
                                <span  class=\"jostHead4\">Current</span>
                                <hr >
                                <div class=\"tokenName jostHead6\">{{coin0Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{currentTotalCoin1}} ({{currentTotalCoin1Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\">{{coin1Name}}</div>
                                <div class=\"tokenResult jostSub2\"> {{currentTotalCoin0}} ({{currentTotalCoin0Price}} \$)</div>
                                <hr >
                                <div class=\"tokenName jostHead6\">Total (BUSD)</div>
                                <div class=\"tokenResult jostSub2\" style=\"font-size: 12px;\">{{currentTotalPrice}} BUSD</div>
                                <hr >
                            </div>
                        <div style=\"text-align: left\">
                            <span  class=\"jostHead3\">Result</span>
                            <hr >
                        </div>
                            <div>
                                <div style=\"display: inline-block; width: 49%; vertical-align: top; text-align: left\">
                                    <div class=\"tokenName jostHead6\">{{coin0Name}}</div>
                                    <div class=\"jostSub2\">
                                        <yieldtext :text=\"calcDiff(currentTotalCoin1, oldTotalCoin1).toFixed(3) \" :number=\"calcDiff(currentTotalCoin1, oldTotalCoin1).toFixed(3)\"></yieldtext> (
                                        <yieldtext :text=\"calcDiff(currentTotalCoin1Price, oldTotalCoin1Price).toFixed(3) + ' \$'\" :number=\"calcDiff(currentTotalCoin1Price, oldTotalCoin1Price).toFixed(3)\"></yieldtext> )
                                    </div>
                                </div>
                                <div style=\"display: inline-block; width: 49%; vertical-align: top\">
                                    <div class=\"tokenName jostHead6\">{{coin1Name}}</div>
                                    <div class=\"jostSub2\">
                                    <yieldtext :text=\"calcDiff(currentTotalCoin0, oldTotalCoin0).toFixed(2) \" :number=\"calcDiff(currentTotalCoin0, oldTotalCoin0).toFixed(2)\"></yieldtext> (
                                    <yieldtext :text=\"calcDiff(currentTotalCoin0Price, oldTotalCoin0Price).toFixed(2) + ' \$'\" :number=\"calcDiff(currentTotalCoin0Price, oldTotalCoin0Price).toFixed(2)\"></yieldtext> )
                                    </div>
                                </div>

                            </div>
                        <div class=\"totalText jostHead3\" style=\"display: inline-block; width: 99%; vertical-align: top; text-align: left\">
                             <span  class=\"jostHead3\">Total:</span>
                            <yieldtext :text=\"calcDiff(currentTotalPrice, oldTotalPrice).toFixed(3) + ' \$'\" :number=\"calcDiff(currentTotalPrice, oldTotalPrice).toFixed(3)\"></yieldtext>
                        </div>
                    </div>
                    <div  class=\"maingraph\">
                            <div class=\"symbolsText jostHead6 text-left\">
                                Farms
                            </div>
                            <div class=\"chart-farm-holder\">
                            <div v-for=\"farm in farms\" class=\"row text-left chart-farm-row jostHead4 pad8\">
                                <div class=\"farm-chart-name\">{{farm.name}}</div> <div class=\"farm-chart-value text-right\"><span v-if=\"farmData[farm.id] && notExpiredToOld(farmData[farm.id])\">{{farmData[farm.id][0].value}}</span></div>
                            </div>
                        </div>
                        <div class=\"pad8\"></div>
                    </div>
                </div>


        </div>

        <br />
    </div>
</div>
</div>
</div>

<script>

    const rawData = '{% endverbatim %}{{replaceWithData|raw}}{% verbatim %}';
    const replaceWithFarmData = '{% endverbatim %}{{ replaceWithFarmData|raw }}{% verbatim %}';

    let decodedData = JSON.parse(rawData);
    let decodedFarmData = JSON.parse(replaceWithFarmData);

    var app = new Vue({
        el: '#mydiv',
        data: {
            allCoinData: [],
            hideCoinValues: false,
            HighestFirst: false,
            limitNumber: 20,
            currentPage: 0,
            searchTerm: \"\",
            favorites: [],
            oldTotalPrice: 0.0,
            oldTotalCoin1: 0.0,
            oldTotalCoin0: 0.0,
            oldTotalCoin1Price: 0.0,
            oldTotalCoin0Price: 0.0,
            currentTotalPrice: 0.0,
            currentTotalCoin1: 0.0,
            currentTotalCoin0: 0.0,
            currentTotalCoin1Price: 0.0,
            currentTotalCoin0Price: 0.0,
            coin0Name: 'coin0',
            coin1Name: 'coin1',
            ilPeriod: 24,
            farms: decodedFarmData,
            currentCoin: {},
            farmValues: {},
            coinsInWallet: 0,
            farmData: []
        },
        components: {
            highcharts: HighchartsVue.Chart
        },
        computed: {
            sortedCoinData() {
                var args = this.allCoinData;
                var sortable = this.allCoinData;

                if (!this.HighestFirst)
                    return sortable
                        .filter((element) => element[0].includes(this.searchTerm))
                        .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

                sortable.sort(function (a, b) {
                    if (parseFloat(a[1].result) >= parseFloat(b[1].result)) return -1;
                    if (parseFloat(a[1].result) <= parseFloat(b[1].result)) return 1;
                    return 0;
                });

                // console.log(news);
                return sortable
                    .filter((element) => element[0].includes(this.searchTerm))
                    .slice(this.limitNumber * this.currentPage, (this.limitNumber * this.currentPage) + this.limitNumber);

            }
        },
        methods: {
            getCurrentPrice(coin) {
                const temp = coin.price[coin.price.length - 1].value;

                if (temp > 10000) {
                    return temp.toFixed(0);
                }

                if (temp > 10) {
                    return temp.toFixed(2);
                }

                if (temp < 0.000001) {
                    return temp.toFixed(10);
                }

                if (temp < 0.01) {
                    return temp.toFixed(6);
                }

                return temp.toFixed(3);
            },
            getCurrenPctResult(coin) {
                if (coin.price === undefined) return 'tbd';
                const now = coin.price[coin.price.length - 1].value;
                const last = coin.price[0].value;
                if (now > last) {
                    var diff = now - last;
                    var gain = diff/now*100;
                } else {
                    var diff = last - now;
                    var gain = -(diff/last*100);
                }
                return gain.toFixed(2);
            },
            addToFavorite(id) {
                if (!isConnected) {
                    alert('Please connect your wallet first');
                    // alert('Please connect your wallet first');
                } else {
                    axios.get('/lp/favorite/' + id + '/' + isConnected)
                        .then(response => {
                            this.favorites.push(id);
                        });
                }
            },
            favoritesLoaded(data) {
                this.startLpFetch();
                for (var favoritekey in data) {
                    this.favorites.push(data[favoritekey].coinLP.id);
                }
            },
            startLpFetch() {
                return ;
                if (YMCurrentAddress === '') {
                    return ;
                }
                isConnected = '0x78d4498970F099e3C94cA8e6Bd90C5795D076041';
                YMCurrentAddress = '0x78d4498970F099e3C94cA8e6Bd90C5795D076041';
                for (farm in this.farms) {
                    // const currentFarm = this.farmAddresses[this.farms[farm].name];
                    const that = this;
                    const farmName = this.farms[farm].name;
                    this.doLpCall(this.farms[farm].name, this.farms[farm].poolNumber, this.farms[farm]).then((res) => {
                        this.farmValues[farmName] = res;
                        this.\$forceUpdate();
                    });

                }

                this.coinsInWallet = this.loadLpInWallet();

            },
            loadLpInWallet: async(network, lpAddress) => {
                if (network === 'Pangolin') {
                    const provider = new ethers.providers.Web3Provider(window.ethereum)
                    const signer = provider.getSigner()
                    const lpAbi = [{\"type\":\"constructor\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"inputs\":[]},{\"type\":\"event\",\"name\":\"Approval\",\"inputs\":[{\"type\":\"address\",\"name\":\"owner\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"spender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Burn\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount0\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Mint\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount0\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Swap\",\"inputs\":[{\"type\":\"address\",\"name\":\"sender\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount0In\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1In\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount0Out\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount1Out\",\"internalType\":\"uint256\",\"indexed\":false},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Sync\",\"inputs\":[{\"type\":\"uint112\",\"name\":\"reserve0\",\"internalType\":\"uint112\",\"indexed\":false},{\"type\":\"uint112\",\"name\":\"reserve1\",\"internalType\":\"uint112\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Transfer\",\"inputs\":[{\"type\":\"address\",\"name\":\"from\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"bytes32\",\"name\":\"\",\"internalType\":\"bytes32\"}],\"name\":\"DOMAIN_SEPARATOR\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"MINIMUM_LIQUIDITY\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"bytes32\",\"name\":\"\",\"internalType\":\"bytes32\"}],\"name\":\"PERMIT_TYPEHASH\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"allowance\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"bool\",\"name\":\"\",\"internalType\":\"bool\"}],\"name\":\"approve\",\"inputs\":[{\"type\":\"address\",\"name\":\"spender\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"balanceOf\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"amount0\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"amount1\",\"internalType\":\"uint256\"}],\"name\":\"burn\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint8\",\"name\":\"\",\"internalType\":\"uint8\"}],\"name\":\"decimals\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"factory\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint112\",\"name\":\"_reserve0\",\"internalType\":\"uint112\"},{\"type\":\"uint112\",\"name\":\"_reserve1\",\"internalType\":\"uint112\"},{\"type\":\"uint32\",\"name\":\"_blockTimestampLast\",\"internalType\":\"uint32\"}],\"name\":\"getReserves\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"initialize\",\"inputs\":[{\"type\":\"address\",\"name\":\"_token0\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"_token1\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"kLast\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"liquidity\",\"internalType\":\"uint256\"}],\"name\":\"mint\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"string\",\"name\":\"\",\"internalType\":\"string\"}],\"name\":\"name\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"nonces\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"permit\",\"inputs\":[{\"type\":\"address\",\"name\":\"owner\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"spender\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"deadline\",\"internalType\":\"uint256\"},{\"type\":\"uint8\",\"name\":\"v\",\"internalType\":\"uint8\"},{\"type\":\"bytes32\",\"name\":\"r\",\"internalType\":\"bytes32\"},{\"type\":\"bytes32\",\"name\":\"s\",\"internalType\":\"bytes32\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"price0CumulativeLast\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"price1CumulativeLast\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"skim\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"swap\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount0Out\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"amount1Out\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"},{\"type\":\"bytes\",\"name\":\"data\",\"internalType\":\"bytes\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"string\",\"name\":\"\",\"internalType\":\"string\"}],\"name\":\"symbol\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[],\"name\":\"sync\",\"inputs\":[],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"token0\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"token1\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"view\",\"payable\":false,\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"totalSupply\",\"inputs\":[],\"constant\":true},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"bool\",\"name\":\"\",\"internalType\":\"bool\"}],\"name\":\"transfer\",\"inputs\":[{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"}],\"constant\":false},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"payable\":false,\"outputs\":[{\"type\":\"bool\",\"name\":\"\",\"internalType\":\"bool\"}],\"name\":\"transferFrom\",\"inputs\":[{\"type\":\"address\",\"name\":\"from\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"to\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"value\",\"internalType\":\"uint256\"}],\"constant\":false}];
                    const lpContract = new ethers.Contract(lpAddress, farmInfo.abi, signer);
                    var userInfo = await lpContract.balanceOf (YMCurrentAddress);

                    var decimals = 18;
                    return userInfo / 10 ** decimals;
                }
            },
            doLpCall: async(network, poolNumber, farm) => {
                return;
                const provider = new ethers.providers.Web3Provider(window.ethereum)
                const signer = provider.getSigner()
                if (network === 'auto' || network === 'pAuto') {
                    const abi = [{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"EmergencyWithdraw\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Withdraw\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"AUTO\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"AUTOMaxSupply\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"AUTOPerBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"AUTOv2\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"contract IERC20\",\"name\":\"_want\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"_strat\",\"type\":\"address\"}],\"name\":\"add\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"burnAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"emergencyWithdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_to\",\"type\":\"uint256\"}],\"name\":\"getMultiplier\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"inCaseTokensGetStuck\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"massUpdatePools\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_inputAmt\",\"type\":\"uint256\"}],\"name\":\"migrateToAUTOv2\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"ownerAUTOReward\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"pendingAUTO\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"contract IERC20\",\"name\":\"want\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"lastRewardBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"accAUTOPerShare\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"strat\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"poolLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"set\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"stakedWantTokens\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"startBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalAllocPoint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"updatePool\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userInfo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"shares\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"rewardDebt\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"withdrawAll\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}];
                    const chefContract = new ethers.Contract(farm.farmAddress, abi, signer);
                    var userInfo = await chefContract.userInfo(poolNumber, YMCurrentAddress);
                    const poolInfo = await chefContract.poolInfo(poolNumber);

                    const autoStratAbi = [{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_govAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_autoFarmAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_AUTOAddress\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_isCAKEStaking\",\"type\":\"bool\"},{\"internalType\":\"bool\",\"name\":\"_isAutoComp\",\"type\":\"bool\"},{\"internalType\":\"address\",\"name\":\"_farmContractAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_wantAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_token0Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_token1Address\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_earnedAddress\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_uniRouterAddress\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Paused\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"account\",\"type\":\"address\"}],\"name\":\"Unpaused\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"AUTOAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"autoFarmAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackRate\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackRateMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buyBackRateUL\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"controllerFee\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"controllerFeeMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"controllerFeeUL\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"convertDustToEarned\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_userAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"earn\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"earnedAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"earnedToAUTOPath\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"earnedToToken0Path\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"earnedToToken1Path\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"entranceFeeFactor\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"entranceFeeFactorLL\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"entranceFeeFactorMax\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"farm\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"farmContractAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"govAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_token\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"}],\"name\":\"inCaseTokensGetStuck\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isAutoComp\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"isCAKEStaking\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"lastEarnBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"onlyGov\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"paused\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"pid\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_controllerFee\",\"type\":\"uint256\"}],\"name\":\"setControllerFee\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_entranceFeeFactor\",\"type\":\"uint256\"}],\"name\":\"setEntranceFeeFactor\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_govAddress\",\"type\":\"address\"}],\"name\":\"setGov\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bool\",\"name\":\"_onlyGov\",\"type\":\"bool\"}],\"name\":\"setOnlyGov\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_buyBackRate\",\"type\":\"uint256\"}],\"name\":\"setbuyBackRate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"sharesTotal\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token0Address\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"token0ToEarnedPath\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token1Address\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"token1ToEarnedPath\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"uniRouterAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"unpause\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wantAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wantLockedTotal\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"wbnbAddress\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_userAddress\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_wantAmt\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]

                    const strat = new ethers.Contract(poolInfo.strat, autoStratAbi, signer);
                    const totalShares = await  strat.sharesTotal();
                    const poolStaked = await  strat.wantLockedTotal() / 1e18;

                    return userInfo.shares / totalShares * poolStaked;
                }else
                if (network === 'TraderJoe') {
                    const abi = [{\"type\":\"constructor\",\"stateMutability\":\"nonpayable\",\"inputs\":[{\"type\":\"address\",\"name\":\"_lyd\",\"internalType\":\"contract LydToken\"},{\"type\":\"address\",\"name\":\"_electrum\",\"internalType\":\"contract ElectrumBar\"},{\"type\":\"address\",\"name\":\"_devaddr\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"_lydPerSec\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_startTimestamp\",\"internalType\":\"uint256\"}]},{\"type\":\"event\",\"name\":\"Deposit\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"pid\",\"internalType\":\"uint256\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"EmergencyWithdraw\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"pid\",\"internalType\":\"uint256\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"OwnershipTransferred\",\"inputs\":[{\"type\":\"address\",\"name\":\"previousOwner\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"SetDevAddress\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"newAddress\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"UpdateEmissionRate\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"_lydPerSec\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Withdraw\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"pid\",\"internalType\":\"uint256\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"BONUS_MULTIPLIER\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"add\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_allocPoint\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"_lpToken\",\"internalType\":\"contract IERC20\"},{\"type\":\"bool\",\"name\":\"_withUpdate\",\"internalType\":\"bool\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"deposit\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"dev\",\"inputs\":[{\"type\":\"address\",\"name\":\"_devaddr\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"devaddr\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract ElectrumBar\"}],\"name\":\"electrum\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"emergencyWithdraw\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"enterStaking\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"getMultiplier\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_from\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_to\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"leaveStaking\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract LydToken\"}],\"name\":\"lyd\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"lydPerSec\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"massUpdatePools\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"owner\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"pendingLyd\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"_user\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"lpToken\",\"internalType\":\"contract IERC20\"},{\"type\":\"uint256\",\"name\":\"allocPoint\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"lastRewardTimestamp\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"accLydPerShare\",\"internalType\":\"uint256\"}],\"name\":\"poolInfo\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"poolLength\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"renounceOwnership\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"set\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_allocPoint\",\"internalType\":\"uint256\"},{\"type\":\"bool\",\"name\":\"_withUpdate\",\"internalType\":\"bool\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"startTimestamp\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"totalAllocPoint\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"transferOwnership\",\"inputs\":[{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"updateEmissionRate\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_lydPerSec\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"updateMultiplier\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"multiplierNumber\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"updatePool\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"rewardDebt\",\"internalType\":\"uint256\"}],\"name\":\"userInfo\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"},{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"withdraw\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_pid\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"_amount\",\"internalType\":\"uint256\"}]}];
                    const chefContract = new ethers.Contract(farmInfo.address, abi, signer);
                    var userInfo = await chefContract.userInfo(poolNumber, YMCurrentAddress);
                    var decimals = 18;
                    return userInfo.amount / 10 ** decimals;
                }else
                if (network === 'Pangolin') {
                    // in farm
                    const pangolinLpABI = [{\"type\":\"constructor\",\"stateMutability\":\"nonpayable\",\"inputs\":[{\"type\":\"address\",\"name\":\"_rewardsToken\",\"internalType\":\"address\"},{\"type\":\"address\",\"name\":\"_stakingToken\",\"internalType\":\"address\"}]},{\"type\":\"event\",\"name\":\"OwnershipTransferred\",\"inputs\":[{\"type\":\"address\",\"name\":\"previousOwner\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\",\"indexed\":true}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Recovered\",\"inputs\":[{\"type\":\"address\",\"name\":\"token\",\"internalType\":\"address\",\"indexed\":false},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"RewardAdded\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"reward\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"RewardPaid\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"reward\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"RewardsDurationUpdated\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"newDuration\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Staked\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"event\",\"name\":\"Withdrawn\",\"inputs\":[{\"type\":\"address\",\"name\":\"user\",\"internalType\":\"address\",\"indexed\":true},{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\",\"indexed\":false}],\"anonymous\":false},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"balanceOf\",\"inputs\":[{\"type\":\"address\",\"name\":\"account\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"earned\",\"inputs\":[{\"type\":\"address\",\"name\":\"account\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"exit\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"getReward\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"getRewardForDuration\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"lastTimeRewardApplicable\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"lastUpdateTime\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"notifyRewardAmount\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"reward\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}],\"name\":\"owner\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"periodFinish\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"recoverERC20\",\"inputs\":[{\"type\":\"address\",\"name\":\"tokenAddress\",\"internalType\":\"address\"},{\"type\":\"uint256\",\"name\":\"tokenAmount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"renounceOwnership\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardPerToken\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardPerTokenStored\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardRate\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewards\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"rewardsDuration\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract IERC20\"}],\"name\":\"rewardsToken\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"setRewardsDuration\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"_rewardsDuration\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"stake\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"stakeWithPermit\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"},{\"type\":\"uint256\",\"name\":\"deadline\",\"internalType\":\"uint256\"},{\"type\":\"uint8\",\"name\":\"v\",\"internalType\":\"uint8\"},{\"type\":\"bytes32\",\"name\":\"r\",\"internalType\":\"bytes32\"},{\"type\":\"bytes32\",\"name\":\"s\",\"internalType\":\"bytes32\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"contract IERC20\"}],\"name\":\"stakingToken\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"totalSupply\",\"inputs\":[]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"transferOwnership\",\"inputs\":[{\"type\":\"address\",\"name\":\"newOwner\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"view\",\"outputs\":[{\"type\":\"uint256\",\"name\":\"\",\"internalType\":\"uint256\"}],\"name\":\"userRewardPerTokenPaid\",\"inputs\":[{\"type\":\"address\",\"name\":\"\",\"internalType\":\"address\"}]},{\"type\":\"function\",\"stateMutability\":\"nonpayable\",\"outputs\":[],\"name\":\"withdraw\",\"inputs\":[{\"type\":\"uint256\",\"name\":\"amount\",\"internalType\":\"uint256\"}]}];
                    const lpContract = new ethers.Contract(farm.coinLP.contractAddr, pangolinLpABI, signer);
                    var userInfo = await lpContract.balanceOf (YMCurrentAddress);

                    var decimals = 18;
                    return userInfo / 10 ** decimals;
                } else {
                    const abi = [{\"inputs\":[{\"internalType\":\"contract CakeToken\",\"name\":\"_cake\",\"type\":\"address\"},{\"internalType\":\"contract SyrupBar\",\"name\":\"_syrup\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_devaddr\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_cakePerBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_startBlock\",\"type\":\"uint256\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Deposit\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"EmergencyWithdraw\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"uint256\",\"name\":\"pid\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Withdraw\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"BONUS_MULTIPLIER\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"contract IBEP20\",\"name\":\"_lpToken\",\"type\":\"address\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"add\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"cake\",\"outputs\":[{\"internalType\":\"contract CakeToken\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"cakePerBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"deposit\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_devaddr\",\"type\":\"address\"}],\"name\":\"dev\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"devaddr\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"emergencyWithdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"enterStaking\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_from\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_to\",\"type\":\"uint256\"}],\"name\":\"getMultiplier\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"leaveStaking\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"massUpdatePools\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"migrate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"migrator\",\"outputs\":[{\"internalType\":\"contract IMigratorChef\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"_user\",\"type\":\"address\"}],\"name\":\"pendingCake\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"poolInfo\",\"outputs\":[{\"internalType\":\"contract IBEP20\",\"name\":\"lpToken\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"lastRewardBlock\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"accCakePerShare\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"poolLength\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_allocPoint\",\"type\":\"uint256\"},{\"internalType\":\"bool\",\"name\":\"_withUpdate\",\"type\":\"bool\"}],\"name\":\"set\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"contract IMigratorChef\",\"name\":\"_migrator\",\"type\":\"address\"}],\"name\":\"setMigrator\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"startBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"syrup\",\"outputs\":[{\"internalType\":\"contract SyrupBar\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalAllocPoint\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"multiplierNumber\",\"type\":\"uint256\"}],\"name\":\"updateMultiplier\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"}],\"name\":\"updatePool\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"},{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userInfo\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"rewardDebt\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_pid\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"}],\"name\":\"withdraw\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}];
                    const chefContract = new ethers.Contract(farm.farmAddress, abi, signer);
                    var userInfo = await chefContract.userInfo(poolNumber, YMCurrentAddress);
                    var decimals = 18;
                    return userInfo.amount / 10 ** decimals;
                }


                return 0;
                // const STAKING_POOL = new ethereum.Contract(farmInfo['address'], farmInfo['abi']);
                // const stakeTokenAddress = STAKING_POOL.userInfo(poolIndex, YMCurrentAddress);
                //  console.log(stakeTokenAddress)
                // var esta= .Contract(farmInfo['abi'], farmInfo['address']);
                //  console.log(ethereum);

                // const web3 = new Web3(\"hhttps://bsc-dataseed.binance.org\")

                //  var test = new web3.eth.Contract(farmInfo['abi'], farmInfo['address']);
                // console.log(test);
            },
            ilPeriodChanged(event) {
                console.log(event.target.value);
                this.ilPeriod = event.target.value;
                this.calculateIL();
            },
            loadLpBalances() {

            },
            calcDiff(var1, var2) {
                return var1 - var2;
            },
            calcDiffHold(var1, var2) {
                return (this.oldTotalCoin0 * 2);
            },
            calculateIL() {
                const hoursAgo = this.ilPeriod;

                const coin = this.allCoinData[0][1];

                this.coin0Name = coin.symbol0Name;
                this.coin1Name = coin.symbol1Name;

                const latestTS = coin.price[coin.price.length - 1]['time'];

                const yest = latestTS - (hoursAgo * 3600);
                //find con from yest

                this.oldTotalPrice = this.findDateTimeObjectPrice(coin.price, yest).toFixed(2);
                this.oldTotalCoin1 = this.findDateTimeObject(coin.coins1, yest).toFixed(7);
                this.oldTotalCoin0 = this.findDateTimeObject(coin.coins0, yest).toFixed(7);
                this.oldTotalCoin1Price = this.findDateTimeObject(coin.coins0price, yest).toFixed(2);
                this.oldTotalCoin0Price = this.findDateTimeObject(coin.coins1price, yest).toFixed(2);
                this.currentTotalPrice = coin.price[coin.price.length - 1]['value'].toFixed(3);
                this.currentTotalCoin1 = coin.coins1[coin.coins1.length - 1]['value'].toFixed(7);
                this.currentTotalCoin0 = coin.coins0[coin.coins0.length - 1]['value'].toFixed(7);
                this.currentTotalCoin1Price = coin.coins0price[coin.coins0price.length - 1]['value'].toFixed(2);
                this.currentTotalCoin0Price = coin.coins1price[coin.coins1price.length - 1]['value'].toFixed(2);
                console.log(this.oldTotalPrice);
                console.log(this.currentTotalPrice);
            },
            findDateTimeObject(array, tofind) {
                for (i = array.length - 1; i > 0; i--) {
                    const now = array[i];

                    if (now['time'] < tofind) {
                        return array[i]['value'];
                    }
                }

                return 0;
            },
            findDateTimeObjectPrice(array, tofind) {
                for (i = array.length - 1; i > 0; i--) {
                    const now = array[i];

                    if (now['time'] < tofind) {
                        return now['value'];
                    }
                }
                return 0;
            },
            doSearch(event) {
                if (event.target.value.length === 0) {
                    this.searchTerm = '';
                }

                if (event.target.value.length >= 3) {
                    this.searchTerm = event.target.value.toUpperCase();
                }
            },
            changeAmountPerPage(event) {
                this.currentPage = 0;
                this.limitNumber = event.target.value;
            },
            getChartOptions(coin, title = 'Price per share', ytext = 'USD') {
                return {
                    chart: {
                        zoomType: 'x',
                        defaultSeriesType: 'area',
                        backgroundColor: 'rgba(0,0,0,0.0)'
                    },
                    title: {
                        text: title
                    },
                    /* subtitle: {
                         text: document.ontouchstart === undefined ?
                             'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                     },*/
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: ytext
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        data: coin
                    }]
                }
            },
            fetchNextData(page) {
                /*  axios.get(\"/getprices/\" + page)
                      .then(response =>  {
                          let amountReturned = 1;
                          for (var lpName in response.data) {
                              amountReturned++;
                              this.allCoinData.push([lpName, response.data[lpName]]);
                          }

                          if (amountReturned > 10) {
                              this.fetchNextData(page+1);
                          }
                      });*/
            }
        },
        mounted() {
            // this.fetchNextData(0);
            for (var lpName in decodedData) {
                this.allCoinData.push([lpName, decodedData[lpName]]);
                this.currentCoin = decodedData[lpName];
                this.calculateIL();
            }
            // setTimeout(this.startLpFetch(),1000);
        }
    })



    async function loadMultipleAvaxSynthetixPools(App, tokens, prices, pools) {
        let totalStaked  = 0, totalUserStaked = 0, individualAPRs = [];
        const infos = await Promise.all(pools.map(p =>
            loadAvaxSynthetixPoolInfo(App, tokens, prices, p.abi, p.address, p.rewardTokenFunction, p.stakeTokenFunction)));
        for (const i of infos) {
            let p = await printSynthetixPool(App, i, \"avax\");
            totalStaked += p.staked_tvl || 0;
            totalUserStaked += p.userStaked || 0;
            if (p.userStaked > 0) {
                individualAPRs.push(p.userStaked * p.apr / 100);
            }
        }
        let totalApr = totalUserStaked == 0 ? 0 : individualAPRs.reduce((x,y)=>x+y, 0) / totalUserStaked;
        return { staked_tvl : totalStaked, totalUserStaked, totalApr };
    }
{% endverbatim %}
</script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        var elems = document.querySelectorAll('select');
        var instances = M.FormSelect.init(elems, {});
    });
</script>
{% include 'layout/footer.twig' %}", "pages/detail.twig", "/var/www/default/templates/pages/detail.twig");
    }
}
