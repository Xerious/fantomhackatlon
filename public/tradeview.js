Vue.component('tradeview', {
    data: function () {
        return {
            count: 0,
            connected: false,
            currentAddress: "",
            pathname: "",
            intData: [],
            showuserpricedata:  true,
            showuserlpdata: false,
            showmaindata: false
        }
    },
    watch: {
        pricedata: function (newVal, oldVal) {
            //  alert('oi123');
            //  console.log('oi, repoint yooo')
            this.drawChart(newVal);
        },
    },
    props: {
        symbol: {
            default: 'AAPL',
            type: String,
        },
        interval: {
            default: 'D',
            type: String,
        },
        containerid: {
            default: 'tv_chart_container',
            type: String,
        },
        datafeedUrl: {
            default: 'https://demo_feed.tradingview.com',
            type: String,
        },
        libraryPath: {
            default: '/charting_library/',
            type: String,
        },
        chartsStorageUrl: {
            default: 'https://saveload.tradingview.com',
            type: String,
        },
        chartsStorageApiVersion: {
            default: '1.1',
            type: String,
        },
        clientId: {
            default: 'tradingview.com',
            type: String,
        },
        userId: {
            default: 'public_user_id',
            type: String,
        },
        fullscreen: {
            default: false,
            type: Boolean,
        },
        autosize: {
            default: true,
            type: Boolean,
        },
        studiesOverrides: {
            type: Object,
        },
        wallet: {
            default: 0,
            type: Number,
        },
        pricedata: [],
        userpricedata: [],
        userlpdata: [],

        width: Number,
        height: Number,
    },
    tvWidget: null,
    methods: {
        showHidePrice() {
          if (this.showuserpricedata){
              this.showuserpricedata = false;
              this.drawChart(this.pricedata);
              return;
          }
            this.showuserpricedata = true;
            this.drawChart(this.pricedata);
        },
        showHideLpTotal() {
          if (this.showuserlpdata){
              this.showuserlpdata = false;
              this.drawChart(this.pricedata);
              return;
          }
            this.showuserlpdata = true;
            this.drawChart(this.pricedata);
        },
        showMainTotal() {
          if (this.showmaindata){
              this.showmaindata = false;
              this.drawChart(this.pricedata);
              return;
          }
            this.showmaindata = true;
            this.drawChart(this.pricedata);
        },
        drawChart(data) {
            if (this.tvWidget !== null && this.tvWidget !== undefined) {
                this.tvWidget.remove();
                this.tvWidget = null;
            }
            const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
            const widgetOptions = {
                symbol: this.symbol,
                width: this.width ?? 300,
                height: this.height ?? 180,
                // BEWARE: no trailing slash is expected in feed URL
                // datafeed: new window.Datafeeds.UDFCompatibleDatafeed(this.datafeedUrl),
                // interval: this.interval,
                container_id: this.containerid,
                //  library_path: this.libraryPath,
                locale: 'en',
                disabled_features: ['use_localstorage_for_settings'],
                enabled_features: ['study_templates'],
                //  charts_storage_url: this.chartsStorageUrl,
                //  charts_storage_api_version: this.chartsStorageApiVersion,
                //  client_id: this.clientId,
                //  user_id: this.userId,
                //fullscreen: this.fullscreen,
                //  autosize: this.autosize,
                // studies_overrides: this.studiesOverrides,
                topColor: 'rgba(33, 150, 243, 0.56)',
                bottomColor: 'rgba(33, 150, 243, 0.04)',
                lineColor: 'rgba(0, 140, 211, 1)',
                lineWidth: 1,
                priceScale: {
                    borderVisible: true,

                },
                timeScale: {
                    borderVisible: true,
                    timeVisible: true,
                    secondsVisible: false,
                    tickMarkFormatter: (time) => {
                        const date = new Date(time * 1000);
                        let min = date.getMinutes();

                        if (min < 10) {
                            min = '0' + min;
                        }

                        return date.getDate() + ' ' + monthNames[date.getMonth()] + '' + '(' + date.getHours() + ':' + min + ')';

                    },
                },
            };
            var darkTheme = {
                chart: {
                    layout: {
                        backgroundColor: '#2B2B43',
                        lineColor: '#2B2B43',
                        textColor: '#D9D9D9',
                    },
                    watermark: {
                        color: 'rgba(0, 0, 0, 0)',
                    },
                    crosshair: {
                        color: '#758696',
                    },
                    grid: {
                        vertLines: {
                            color: '#2B2B43',
                        },
                        horzLines: {
                            color: '#363C4E',
                        },
                    },
                },
                /*   series: {
                       topColor: 'rgba(32, 226, 47, 0.56)',
                       bottomColor: 'rgba(32, 226, 47, 0.04)',
                       lineColor: 'rgba(32, 226, 47, 1)',
                   },*/
            };

            const lightTheme = {
                chart: {
                    layout: {
                        backgroundColor: '#FFFFFF',
                        lineColor: 'rgba(0, 140, 211, 1)',
                        textColor: '#191919',
                    },
                    watermark: {
                        color: 'rgba(0, 0, 0, 0)',
                    },
                    grid: {
                        vertLines: {
                            visible: false,
                        },
                        horzLines: {
                            color: '#f0f3fa',
                        },
                    },
                },
                /* series: {
                     topColor: 'rgba(0, 140, 211, 0.80)',
                     bottomColor: 'rgba(0, 140, 211, 0.00)',
                     lineColor: 'rgba(0, 140, 211, 1)',
                 },*/
            };

            const tvWidget = new LightweightCharts.createChart(this.$el.querySelector('.TVChartContainer'), widgetOptions);
            this.tvWidget = tvWidget;
            this.chart = tvWidget;

            if ((data !== undefined && data.length > 0 && this.showmaindata === true) || ((this.userlpdata === undefined || this.userlpdata.length === 0)&& (this.userpricedata === undefined|| this.userpricedata.length === 0)) ) {
               if (this.wallet !== 0) {
                   var areaSeries = tvWidget.addAreaSeries({
                       topColor: 'rgba(0, 206, 220, 1)',
                       bottomColor: 'rgba(0, 206, 220, 0.3)',
                       lineColor: 'rgba(0, 206, 220, 1)',
                       lineWidth: 2,
                   });
               } else {
                   var areaSeries = tvWidget.addAreaSeries({
                       topColor: 'rgba(19, 75, 178, 1)',
                       bottomColor: 'rgba(19, 75, 178, 0.3)',
                       lineColor: 'rgba(19, 75, 189, 1)',
                       lineWidth: 2,
                   });
               }

                areaSeries.setData(data);
            }


            if (this.userlpdata !== undefined && this.userlpdata.length > 0 && this.showuserlpdata === true) {
                var extraSeries = tvWidget.addAreaSeries({
                    topColor: 'rgba(25, 26, 55, 1)',
                    bottomColor: 'rgba(25, 26, 55, 0.85)',
                    lineColor: 'rgba(25, 26, 55, 1)',
                    lineWidth: 2,

                });
                extraSeries.setData(this.userlpdata);
            }


            if (this.userpricedata !== undefined && this.userpricedata.length > 0 && this.showuserpricedata === true) {
                if (this.wallet !== 0) {
                    var extraSeries2 = tvWidget.addAreaSeries({
                        topColor: 'rgba(19, 75, 178, 1)',
                        bottomColor: 'rgba(19, 75, 178, 0.3)',
                        lineColor: 'rgba(19, 75, 189, 1)',
                        lineWidth: 2,
                    });
                } else {
                    var extraSeries2 = tvWidget.addAreaSeries({
                        topColor: 'rgba(0, 206, 220, 1)',
                        bottomColor: 'rgba(0, 206, 220, 0.3)',
                        lineColor: 'rgba(0, 206, 220, 1)',
                        lineWidth: 2,
                    });
                }
                extraSeries2.setData(this.userpricedata);
            }

            // const lineSeries = tvWidget.addLineSeries();
            // lineSeries.setData(data);


            tvWidget.applyOptions(lightTheme.chart);
            // areaSeries.applyOptions(lightTheme.series);


            var toolTip = document.createElement('div');
            toolTip.className = 'floating-tooltip-2';
            this.$el.appendChild(toolTip);

            tvWidget.timeScale().fitContent();

        }
    },
    mounted() {
        if (this.pricedata === undefined) return;
        this.drawChart(this.pricedata);

        return;
        /* const widgetOptions = {
             symbol: this.symbol,
             // BEWARE: no trailing slash is expected in feed URL
            // datafeed: new window.Datafeeds.UDFCompatibleDatafeed(this.datafeedUrl),
             interval: this.interval,
             container_id: this.containerid,
             library_path: this.libraryPath,
             locale: getLanguageFromURL() || 'en',
             disabled_features: ['use_localstorage_for_settings'],
             enabled_features: ['study_templates'],
             charts_storage_url: this.chartsStorageUrl,
             charts_storage_api_version: this.chartsStorageApiVersion,
             client_id: this.clientId,
             user_id: this.userId,
             fullscreen: this.fullscreen,
             autosize: this.autosize,
             studies_overrides: this.studiesOverrides,
         };
         const tvWidget = new LightweightCharts.widget(widgetOptions);
         this.tvWidget = tvWidget;
         tvWidget.onChartReady(() => {
             tvWidget.headerReady().then(() => {
                 const button = tvWidget.createButton();
                 button.setAttribute('title', 'Click to show a notification popup');
                 button.classList.add('apply-common-tooltip');
                 button.addEventListener('click', () => tvWidget.showNoticeDialog({
                     title: 'Notification',
                     body: 'TradingView Charting Library API works correctly',
                     callback: () => {
                         // eslint-disable-next-line no-console
                         console.log('Noticed!');
                     },
                 }));
                 button.innerHTML = 'Check API';
             });
         });*/
    },
    destroyed() {
        if (this.tvWidget !== null) {
            this.tvWidget.remove();
            this.tvWidget = null;
        }
    },
    template: '<div >' +
        '<div><div class="TVChartContainer" :id="containerid" ></div></div>' +
        '<div v-if="userpricedata != undefined && userpricedata.length > 0" class="graphPadding" style="text-align: left">' +
        '   <div style="display: inline-blocK" class="switch wallet-price-switch jostHead4" v-if ="showuserpricedata" @click="showHidePrice()"><label><input type="checkbox" checked><span class="lever"></span>USD total</label></div>' +
        '   <div style="display: inline-blocK" class="switch wallet-price-switch jostHead4" v-if ="!showuserpricedata" @click="showHidePrice()"><label><input type="checkbox"><span class="lever"></span>USD total</label></div>' +
        '   <div style="display: inline-block" class="switch wallet-lp-switch jostHead4" v-if ="showmaindata" @click="showMainTotal()"><label><input type="checkbox" checked><span class="lever"></span>1 LP USD</label></div>' +
        '   <div style="display: inline-block" class="switch wallet-lp-switch jostHead4" v-if ="!showmaindata" @click="showMainTotal()"><label><input type="checkbox"><span class="lever"></span>1 LP USD</label></div>' +
        '   <div style="display: inline-block" class="switch wallet-user-lp-switch jostHead4" v-if ="showuserlpdata" @click="showHideLpTotal()"><label><input type="checkbox" checked><span class="lever"></span>LP Owned</label></div>' +
        '   <div style="display: inline-block" class="switch wallet-user-lp-switch jostHead4" v-if ="!showuserlpdata" @click="showHideLpTotal()"><label><input type="checkbox"><span class="lever"></span>LP Owned</label></div>' +
        '</div>'+
        '</div>'
})
